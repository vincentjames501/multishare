CREATE DATABASE  IF NOT EXISTS `multishare` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `multishare`;
-- MySQL dump 10.13  Distrib 5.5.16, for Win32 (x86)
--
-- Host: localhost    Database: multishare
-- ------------------------------------------------------
-- Server version	5.5.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `USER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(45) NOT NULL,
  `PASSWORD` varchar(45) NOT NULL,
  `ENABLED` tinyint(1) NOT NULL,
  `EMAIL` varchar(100) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'vincentjames501','0123456',1,'vincentjames501@gmail.com'),(2,'DEFAULT_USER','super_top_secret',1,'noreply@multishare.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploaded_file_history`
--

DROP TABLE IF EXISTS `uploaded_file_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploaded_file_history` (
  `uploaded_file_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `uploaded_file_id` int(11) NOT NULL,
  `status` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `file_host_id` int(11) NOT NULL,
  PRIMARY KEY (`uploaded_file_history_id`),
  KEY `uploaded_file_pk` (`uploaded_file_id`),
  KEY `file_upload_host_pk` (`file_host_id`),
  CONSTRAINT `file_upload_host_pk` FOREIGN KEY (`file_host_id`) REFERENCES `file_host` (`file_host_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `uploaded_file_pk` FOREIGN KEY (`uploaded_file_id`) REFERENCES `uploaded_file` (`uploaded_file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploaded_file_history`
--

LOCK TABLES `uploaded_file_history` WRITE;
/*!40000 ALTER TABLE `uploaded_file_history` DISABLE KEYS */;
INSERT INTO `uploaded_file_history` VALUES (2,2,'Successfully Uploaded To FileSonic - Download Link Available At http://www.filesonic.com/file/AQszPEs/01_When_I\'m_Gone.mp3',3),(3,2,'Successfully Uploaded To FileServe - Download Link Available At http://www.fileserve.com/file/HEtdM4B/01 When I\'m Gone.mp3',2),(4,2,'Successfully Uploaded To HotFile - Download Link Available At http://hotfile.com/dl/139775268/7f81374/01_When_Im_Gone.mp3.html',4);
/*!40000 ALTER TABLE `uploaded_file_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uploaded_file`
--

DROP TABLE IF EXISTS `uploaded_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `uploaded_file` (
  `uploaded_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `USER_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`uploaded_file_id`),
  KEY `users_fk` (`USER_ID`),
  CONSTRAINT `users_fk` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uploaded_file`
--

LOCK TABLES `uploaded_file` WRITE;
/*!40000 ALTER TABLE `uploaded_file` DISABLE KEYS */;
INSERT INTO `uploaded_file` VALUES (2,'01 When I\'m Gone.mp3',2);
/*!40000 ALTER TABLE `uploaded_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_host`
--

DROP TABLE IF EXISTS `user_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_host` (
  `user_host_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_host_id` int(11) NOT NULL,
  `USER_ID` int(10) unsigned NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`user_host_id`),
  KEY `file_host_fk` (`file_host_id`),
  KEY `host_fk` (`file_host_id`),
  KEY `user_fk` (`USER_ID`),
  CONSTRAINT `host_fk` FOREIGN KEY (`file_host_id`) REFERENCES `file_host` (`file_host_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_fk` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_host`
--

LOCK TABLES `user_host` WRITE;
/*!40000 ALTER TABLE `user_host` DISABLE KEYS */;
INSERT INTO `user_host` VALUES (1,1,1,'vincentjames501','firestone'),(2,2,1,'vincentjames501','firestone');
/*!40000 ALTER TABLE `user_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `default_host`
--

DROP TABLE IF EXISTS `default_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `default_host` (
  `default_host_id` int(11) NOT NULL,
  `file_host_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`default_host_id`),
  KEY `file_host_pk` (`file_host_id`),
  CONSTRAINT `file_host_pk` FOREIGN KEY (`file_host_id`) REFERENCES `file_host` (`file_host_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `default_host`
--

LOCK TABLES `default_host` WRITE;
/*!40000 ALTER TABLE `default_host` DISABLE KEYS */;
INSERT INTO `default_host` VALUES (1,1,'vincentjames501','firestone'),(2,2,'vincentjames501','firestone'),(3,3,'vincentjames501','firestone'),(4,4,'vincentjames501','firestone'),(5,5,'vincentjames501','firestone'),(6,6,'vincentjames501','firestone'),(7,7,'vincentjames501','firestone'),(8,8,'vincentjames501','firestone'),(9,9,'vincentjames501','firestone'),(10,10,'vincentjames501','firestone');
/*!40000 ALTER TABLE `default_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_host`
--

DROP TABLE IF EXISTS `file_host`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `file_host` (
  `file_host_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`file_host_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_host`
--

LOCK TABLES `file_host` WRITE;
/*!40000 ALTER TABLE `file_host` DISABLE KEYS */;
INSERT INTO `file_host` VALUES (1,'depositfiles'),(2,'fileserve'),(3,'filesonic'),(4,'hotfile'),(6,'megaupload'),(7,'rapidshare'),(8,'uploadhere'),(9,'uploadking'),(5,'wupload'),(10,'zshare');
/*!40000 ALTER TABLE `file_host` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `USER_ROLE_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(10) unsigned NOT NULL,
  `AUTHORITY` varchar(45) NOT NULL,
  PRIMARY KEY (`USER_ROLE_ID`),
  KEY `FK_user_roles` (`USER_ID`),
  CONSTRAINT `FK_user_roles` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,1,'ROLE_USER'),(2,1,'ROLE_ADMIN'),(3,1,'ROLE_USER');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-01-04 11:04:49
