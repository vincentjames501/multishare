<%@ include file="./common/aataglibs.jsp" %>
<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Login</strong>
    </div>
</div>
</header>
<section id="content">
    <br/>
    <center>
        <div id="loginFormDiv">
            <form action="j_spring_security_check" method="POST" data-ajax="false">
                <c:if test="${not empty param.login_error}">
                    <span class="error-msg">Login unsuccessful: Please try again<br><br></span>
                </c:if>
                <p>Please Enter Your Logon Information</p>
                <input type="text" name="j_username" placeholder="Username"/><br/>
                <input type="password" name="j_password" placeholder="Password"/><br/>
                <input type="submit" value="Login"/><br/>
                <a href="forgotPassword.htm">Forgot Password?</a><br/>
                <a href="register.htm">Register For An Account</a><br/>
            </form>
        </div>
    </center>
    <br/>
</section>