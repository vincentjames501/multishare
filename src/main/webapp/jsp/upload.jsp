<%@ include file="./common/aataglibs.jsp" %>
<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Upload</strong>
    </div>
</div>
</header>
<section id="content">
    <div class="bg-1">
        <div class="main">
            <div class="wrapper">
                <div class="box-1 fleft">
                    <strong class="title-1">Single</strong>
                    <strong class="title-2">File</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img1.png" alt=""/></figure>
                    <a class="button" href="singleFileUpload.htm">Upload now</a>
                </div>
                <div class="box-2 fleft">
                    <strong class="title-1">Multiple</strong>
                    <strong class="title-2">Files</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img4.png" alt=""/></figure>
                    <a class="button" href="multiFileUpload.htm">Upload Now</a>
                </div>
                <div class="box-2 fleft">
                    <strong class="title-1">Remote</strong>
                    <strong class="title-2">File</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img1.png" alt=""/></figure>
                    <a class="button" href="remoteFileUpload.htm">Upload Now</a>
                </div>
                <div class="box-3 fleft">
                    <strong class="title-1">Remote</strong>
                    <strong class="title-2">Files</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img4.png" alt=""/></figure>
                    <a class="button" href="multiRemoteFileUpload.htm">Upload Now</a>
                </div>
            </div>
        </div>
    </div>
</section>