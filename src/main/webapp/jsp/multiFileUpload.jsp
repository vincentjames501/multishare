<%@ include file="./common/aataglibs.jsp" %>
<link rel="stylesheet" href="inc/js/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css" media="screen" />
<title>Plupload - Events example</title>
<style type="text/css">
    body {
        font-family:Verdana, Geneva, sans-serif;
        font-size:13px;
        color:#333;
        background:url(inc/js/plupload/examples/bg.jpg);
    }
</style>

<%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>--%>
<script type="text/javascript" src="http://bp.yahooapis.com/2.4.21/browserplus-min.js"></script>

<script type="text/javascript" src="inc/js/plupload/js/plupload.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/plupload.gears.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/plupload.silverlight.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/plupload.flash.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/plupload.browserplus.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/plupload.html4.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/plupload.html5.js"></script>
<script type="text/javascript" src="inc/js/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Single File Upload</strong>
    </div>
</div>
</header>
<section id="content">
    <div class="bg-2">
        <div class="main">
            <div class="wrapper">
                <article class="col-1">
                    <div id="uploader" style="width: 850px; height: 350px;">You browser doesn't support upload.</div>
                    <table width="850px">
                        <tr>
                            <td><input type="checkbox" name="uploadTo" value="depositfiles" /><img src="inc/images/hosts/depositfiles.gif"/></td>
                            <td><input type="checkbox" name="uploadTo" value="filesonic" /><img src="inc/images/hosts/filesonic.gif"/></td>
                            <td><input type="checkbox" name="uploadTo" value="fileserve" /><img src="inc/images/hosts/fileserve.gif"/></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="uploadTo" value="hotfile" /><img src="inc/images/hosts/hotfile.gif"/></td>
                            <td><input type="checkbox" name="uploadTo" value="megaupload" /><img src="inc/images/hosts/megaupload.gif"/></td>
                            <td><input type="checkbox" name="uploadTo" value="rapidshare" /><img src="inc/images/hosts/rapidshare.gif"/></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="uploadTo" value="uploadhere" /><img src="inc/images/hosts/uploadhere.gif"/></td>
                            <td><input type="checkbox" name="uploadTo" value="uploadking" /><img src="inc/images/hosts/uploadking.gif"/></td>
                            <td><input type="checkbox" name="uploadTo" value="wupload" /><img src="inc/images/hosts/wupload.gif"/></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="uploadTo" value="zshare" /><img src="inc/images/hosts/zshare.gif"/></td>
                        </tr>
                    </table>
                </article>
            </div>
        </div>
    </div>
</section>




<script type="text/javascript">
    var fileHosts;
    $(function() {
        $("#uploader").pluploadQueue({
            runtimes: 'html5,gears,browserplus,silverlight,flash,html4',
            url: '/singleFileUpload.htm',
            max_file_size: '1024mb',
            chunk_size: '1mb',
            unique_names: true,
            multi_selection : true,
            multipart: true,

            // Resize images on clientside if we can
            //resize: {width: 750, height: 250, quality: 100},

            // Flash/Silverlight paths
            flash_swf_url: 'inc/js/plupload/js/plupload.flash.swf',
            silverlight_xap_url: 'inc/js/plupload/js/plupload.silverlight.xap',

            // PreInit events, bound before any internal events
            preinit: {
                Init: function(up, info) {
                },

                UploadFile: function(up, file) {
                    fileHosts = "";
                    var first = true;
                    $("input[name='uploadTo']:checked").each(function(i, val){
                        if(first) {
                            fileHosts += val.value;
                            first = false
                        }else {
                            fileHosts += ","+val.value;
                        }
                    });
                    up.settings.multipart_params = {name: file.name, UUID: file.id, fileHosts: fileHosts, size: file.size};
                }
            },
            init: {
                Refresh: function(up) {
                },

                StateChanged: function(up) {
                },

                QueueChanged: function(up) {
                },

                UploadProgress: function(up, file) {
                },

                FilesAdded: function(up, files) {
                },

                FilesRemoved: function(up, files) {
                },

                FileUploaded: function(up, file, info) {
                },

                ChunkUploaded: function(up, file, info) {
                },

                Error: function(up, args) {
                }
            }
        });
    });
</script>