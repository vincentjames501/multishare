<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">DMCA</strong>
    </div>
</div>
</header>
<section id="content">
<div class="bg-2">
	<div class="main">
		<div class="wrapper">
			<article class="col-1">
				<strong class="title-3">DMCA Form</strong>
				We take abuse of our service very seriously. If you wish to report a copyright infringement, we need you to send us a proper notification. A proper notification MUST have at least the following information, or it may be IGNORED: <br/><br/>

1: Identify yourself as either:<br/>
a: The owner of a copyrighted work(s), or <br/>
b: A person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. <br/>
<br/><br/>
2: State your contact information, including your TRUE NAME, street address, telephone number, and email address. 
<br/><br/>
3: Identify the copyrighted work that you believe is being infringed, or if a large number of works are appearing at a single website, a representative list of the works. 
<br/><br/>
4: Identify the material that you claim is infringing your copyrighted work, to which you are requesting that Multiupload disable access over the World Wide Web. 
<br/><br/>
5: Identify the material by its URL(s).
<br/><br/>
6: State that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agents, or the law.
<br/><br/>
7: State that the information in the notice is accurate, under penalty of perjury. 
<tbody><tr><td width="50%" valign="top"><table style="font-size:12px; font-weight:bold; font-family:arial;"><tbody><tr><td height="50"> Link to material:<br><input type="text" style="border:1px solid; border-color:#B0B8BB; width:367px; height:21px;" value="http://" name="link" id="link"></td></tr><tr><td height="50"> Your name:<br><input type="text" style="border:1px solid; border-color:#B0B8BB; width:367px; height:21px;" name="name" id="name"></td></tr><tr><td height="50"> Your e-mail address:<br><input type="text" style="border:1px solid; border-color:#B0B8BB; width:367px; height:21px;" name="email" id="email"></td></tr><tr><td height="50"> Organisation:<br><input type="text" style="border:1px solid; border-color:#B0B8BB; width:367px; height:21px;" name="organisation" id="organisation"></td></tr></tbody></table></td><td valign="top" align="right"><table style="font-size:12px; font-weight:bold; font-family:arial;"><tbody><tr><td valign="top"> Description of abuse:<br><textarea style="border:1px solid; border-color:#B0B8BB; width:367px; height:150px;" name="description" id="description"></textarea></td></tr><tr><td align="right"><input type="button" style="border:1px solid; border-color:#B0B8BB; background-color:#FFFFFF; width:175px; margin-left:2px; height:21px;" value="Report abuse" onclick="postdmca();" id="sendbutton"></td></tr></tbody></table></td></tr></tbody>
			</article>
		</div>
	</div>
</div>
</section>
    