<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Contact Us</strong>
    </div>
</div>
</header>
<section id="content">
<div class="bg-2">
	<div class="main">
		<div class="wrapper">
			<article class="col-1">
				<strong class="title-3">Contact Form</strong>
				<form id="contact-form" method="post" enctype="multipart/form-data">                    
					<fieldset>
						<div class="wrapper">
							<label><span class="text-form">Your Name:</span><input name="p1" type="text" /></label>
							<label><span class="text-form">Your E-mail:</span><input name="p2" type="text" /></label>   
							<div class="wrapper"><div class="text-form">Your Message:</div><textarea></textarea></div>
							<div class="buttons">
								<a class="button" href="#" onClick="document.getElementById('contact-form').reset()">Clear</a>
								<a class="button" href="#" onClick="document.getElementById('contact-form').submit()">Send</a>
							</div>    
						</div>                                    
					</fieldset>						
				</form>
			</article>
		</div>
	</div>
</div>
</section>