<%@ include file="./common/aataglibs.jsp" %>
<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Link Accounts</strong>
    </div>
</div>
</header>
<section id="content">
    <select id="hostSelect">
        <option value="depositfiles">depositfiles</option>
        <option value="filesonic">filesonic</option>
        <option value="fileserve">fileserve</option>
        <option value="hotfile">hotfile</option>
        <option value="megaupload">megaupload</option>
        <option value="rapidshare">rapidshare</option>
        <option value="uploadhere">uploadhere</option>
        <option value="uploadking">uploadking</option>
        <option value="wupload">wupload</option>
        <option value="zshare">zshare</option>
    </select><br />
    Username: <input type="text" name="username"/><br/>
    Password: <input type="password" name="password"/><br/>
    <input id="submitButton" type="submit" value="Submit"/>
    <script type="text/javascript">
        $("#submitButton").click(function(){
            $.ajax({
                url:"/updateAccountInformation.htm",
                type:"POST",
                data:{username:$("input[name = 'username']").val(),
                    password:$("input[name = 'password']").val(),
                    host:$("#hostSelect").val()}
            });
        });
    </script>
</section>