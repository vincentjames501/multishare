<%@ include file="./common/aataglibs.jsp" %>
<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Password Recovery</strong>
    </div>
</div>
</header>
<section id="content">
    <br/>
    <center>
        <div id="loginFormDiv">
            <form method="POST">
                <c:if test="${not empty errorMessage}">
                    <span class="error-msg">${errorMessage}<br><br></span>
                </c:if>
                <c:if test="${not empty successMessage}">
                    <span class="success-msg">${successMessage}<br><br></span>
                </c:if>
                <p>Registration (please enter username or password)</p>
                <label for="username">Username</label>
                <input type="text" name="username" id="username" minlength="6"/><br/>
                <label for="mail">Email Address</label>
                <input  tabindex="2" type="email" name="mail" id="mail" /><br/>
                <input type="submit" value="Recover Password"/>
            </form>
        </div>
    </center>
    <br/>
</section>