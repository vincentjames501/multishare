<%@ include file="./common/aataglibs.jsp" %>
<script src="inc/js/jqdock/jquery.jqDock.min.js" type="text/javascript"></script>
<div class="row-bot">
    <div class="main">
        <strong class="news-title fleft">Account</strong>
    </div>
</div>
</header>
<section id="content">
    <div id='menu' style="float: left;">
        <img id="link" src='inc/images/accounts/link.png' alt='inc/images/accounts/link.png' title='Link' />
        <img id="settings" src='inc/images/accounts/settings.png' alt='inc/images/accounts/settings.png' title='Settings' />
        <img id="upgrade" src='inc/images/accounts/upgrade.png' alt='inc/images/accounts/upgrade.png' title='Upgrade' />
        <img id="history" src='inc/images/accounts/history.png' alt='inc/images/accounts/history.png' title='History' />
    </div>
    <div id="accountContent" style="float: left;">

    </div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        $('#menu').jqDock({align:'left', labels:'bc', size:80});
        $('#link').click(function(){
            $("#accountContent").load('/linkAccounts.htm');
        });
        $('#settings').click(function(){
            $("#accountContent").load('/settings.htm');
        });
        $('#upgrade').click(function(){
            $("#accountContent").load('/upgrade.htm');
        });
        $('#history').click(function(){
            $("#accountContent").load('/history.htm');
        });
    });
</script>