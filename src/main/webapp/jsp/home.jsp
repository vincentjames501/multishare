<div class="row-bot">
    <strong class="slog-1">MultiShare Is Proud</strong>
    <strong class="slog-2">To&nbsp;Offer&nbsp;Fresh, New&nbsp;Ways</strong>
    <strong class="slog-3">To Distribute Your Files</strong>
</div>
</header>
<section id="content">
    <div class="bg-1">
        <div class="main">
            <div class="wrapper">
                <div class="box-1 fleft">
                    <strong class="title-1">Single</strong>
                    <strong class="title-2">File</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img1.png" alt=""/></figure>
                    <a class="button" href="singleFileUpload.htm">Upload now</a>
                </div>
                <div class="box-2 fleft">
                    <strong class="title-1">Multiple</strong>
                    <strong class="title-2">Files</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img4.png" alt=""/></figure>
                    <a class="button" href="multiFileUpload.htm">Upload Now</a>
                </div>
                <div class="box-2 fleft">
                    <strong class="title-1">Remote</strong>
                    <strong class="title-2">File</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img1.png" alt=""/></figure>
                    <a class="button" href="remoteFileUpload.htm">Upload Now</a>
                </div>
                <div class="box-3 fleft">
                    <strong class="title-1">Remote</strong>
                    <strong class="title-2">Files</strong>
                    <figure class="indent-bot"><img src="inc/images/page1-img4.png" alt=""/></figure>
                    <a class="button" href="multiRemoteFileUpload.htm">Upload Now</a>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-2">
        <div class="main">
            <div class="wrapper">
                <article class="col-1">
                    <h3>What is MultiShare?</h3>
                    <h4 class="img-indent-bot">The quick way to upload and distribute your files across multiple
                        services.</h4>

                    <p class="indent-bot"><strong>MultiShare</strong> is your new one stop solution to quickly and
                        easily uploading and sharing all of your files. Finally, a solution that allows your to back up
                        all of your files across multiple services.</p>
                    <a class="button" href="#">Read More</a>
                </article>
                <article class="col-2">
                    <h3 class="border-bot indent-bot">What's New?</h3>

                    <div class="wrapper indent-bot">
                        <figure class="img-indent2 frame"><img src="inc/images/page1-img5.jpg" alt=""/></figure>
                        <div class="extra-wrap">
                            <time datetime="2011-06-07" class="tdate-1"><a href="#">Tue, June 7, 2011</a></time>
                            Open &nbsp;<a class="link-1" href="#">[...]</a>
                        </div>
                    </div>
                    <div class="wrapper indent-bot">
                        <figure class="img-indent2 frame"><img src="inc/images/page1-img6.jpg" alt=""/></figure>
                        <div class="extra-wrap">
                            <time datetime="2011-06-05" class="tdate-1"><a href="#">Sun, June 5, 2011</a></time>
                            Open &nbsp;<a class="link-1" href="#">[...]</a>
                        </div>
                    </div>
                    <div class="wrapper">
                        <figure class="img-indent2 frame"><img src="inc/images/page1-img7.jpg" alt=""/></figure>
                        <div class="extra-wrap">
                            <time datetime="2011-06-02" class="tdate-1"><a href="#">Thu, June 2, 2011</a></time>
                            Open &nbsp;<a class="link-1" href="#">[...]</a>
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>