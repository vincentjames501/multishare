<%@ include file="./common/aataglibs.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title></title>
    <meta charset="utf-8">
    <link href='http://fonts.googleapis.com/css?family=Lora' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="inc/css/reset.css" type="text/css" media="screen">
    <link rel="stylesheet" href="inc/css/style.css" type="text/css" media="screen">
    <link rel="stylesheet" href="inc/css/layout.css" type="text/css" media="screen">
    <link rel="stylesheet" href="inc/css/font.css" type="text/css" media="screen">
    <script src="../inc/js/jquery-1.4.4.js" type="text/javascript"></script>
    <%--<script src="../inc/js/cufon-yui.js" type="text/javascript"></script>--%>
    <%--<script src="../inc/js/cufon-replace.js" type="text/javascript"></script>--%>
    <%--<script src="../inc/js/Lora_400.font.js" type="text/javascript"></script>--%>
    <script src="../inc/js/FF-cash.js" type="text/javascript"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
	<!--[if lt IE 7]>
        <div style=' clear: both; text-align:center; position: relative;'>
            <a href="http://www.microsoft.com/windows/internet-explorer/default.aspx?ocid=ie6_countdown_bannercode"><img src="http://www.theie6countdown.com/images/upgrade.jpg" border="0"  alt="" /></a>
        </div>
	<![endif]-->
    <!--[if lt IE 9]>
   		<script type="text/javascript" src="js/html5.js"></script>
        <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen">
	<![endif]-->
</head>
<body <c:if test="${layoutPage == 'home.jsp'}">id="page1"</c:if><c:if test="${layoutPage != 'home.jsp'}">id="page2"</c:if>>
    <header>
    	<div class="row-top">
        	<div class="main">
                <div class="wrapper">
                    <h1><a style="text-decoration: none;" href="index.htm"><span style="height:250px;float:left;padding-top:22px;font-size:400%"><span style="color: black;">Multi<span><span style="color: #C53005;">Share</span></span></a></h1>
                    <nav>
                        <ul class="menu">
                          	<li><a <c:if test="${layoutPage == 'home.jsp'}">class="active"</c:if> href="index.htm">Home</a></li>
                            <li><a <c:if test="${layoutPage == 'upload.jsp'}">class="active"</c:if> href="upload.htm">Upload</a></li>
                            <%if (request.isUserInRole("ROLE_USER")) { %>
                            <li><a <c:if test="${layoutPage == 'account.jsp'}">class="active"</c:if> href="account.htm">Account</a></li>
                            <li><a href="/logout">Log Out</a></li>
                            <%}%>
                            <%if (!request.isUserInRole("ROLE_USER")) { %>
                            <li><a <c:if test="${layoutPage == 'login.jsp'}">class="active"</c:if> href="login.htm">Login</a></li>
                            <li><a <c:if test="${layoutPage == 'register.jsp'}">class="active"</c:if> href="register.htm">Register</a></li>
                            <%}%>
                            <li><a <c:if test="${layoutPage == 'contact.jsp'}">class="active"</c:if> href="contact.htm">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
		<jsp:include page="${layoutPage}"/>
    <footer>
        <div class="main">
        	<div class="wrapper">
            	<div class="col-1">
                	<strong class="phone-numb"><strong>DMCA</strong> <a href="dmca.htm">here</a></strong>
                    <div><a rel="nofollow" href="http://www.multishare.com" target="_blank">MultiShare</a> by multishare.com</div>
                    <div><!-- {%FOOTER_LINK} --></div>
                </div>
                <div class="col-2">
                	<strong class="social-title">Follow Us</strong>
                    <div class="wrapper">
                        <ul class="list-services">
                            <li><a class="item-1" href="#"></a></li>
                            <li><a class="item-2" href="#"></a></li>
                            <li><a class="item-3" href="#"></a></li>
                            <li><a class="item-4" href="#"></a></li>
                            <li><a class="item-5" href="#"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<%--<script type="text/javascript"> Cufon.now(); </script>--%>
</body>
</html>
