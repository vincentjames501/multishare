package multishare.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 9:34 PM
 */
@javax.persistence.Table(name = "uploaded_file", schema = "", catalog = "multishare")
@Entity
public class EntityUploadedFile {
    private Integer uploadedFileId;

    @javax.persistence.Column(name = "uploaded_file_id")
    @Id
    @GeneratedValue
    public Integer getUploadedFileId() {
        return uploadedFileId;
    }

    public void setUploadedFileId(Integer uploadedFileId) {
        this.uploadedFileId = uploadedFileId;
    }

    private String name;

    @javax.persistence.Column(name = "name")
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityUploadedFile that = (EntityUploadedFile) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (uploadedFileId != null ? !uploadedFileId.equals(that.uploadedFileId) : that.uploadedFileId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uploadedFileId != null ? uploadedFileId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    private Collection<EntityUploadedFileHistory> uploadedFileHistoriesByUploadedFileId;

    @OneToMany(mappedBy = "uploadedFileByUploadedFileId")
    public Collection<EntityUploadedFileHistory> getUploadedFileHistoriesByUploadedFileId() {
        return uploadedFileHistoriesByUploadedFileId;
    }

    public void setUploadedFileHistoriesByUploadedFileId(Collection<EntityUploadedFileHistory> uploadedFileHistoriesByUploadedFileId) {
        this.uploadedFileHistoriesByUploadedFileId = uploadedFileHistoriesByUploadedFileId;
    }

    private EntityUsers user;

    @ManyToOne
    @javax.persistence.JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    public EntityUsers getUser() {
        return user;
    }

    public void setUser(EntityUsers user) {
        this.user = user;
    }
}
