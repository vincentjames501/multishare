package multishare.entities;

import multishare.common.Host;

import javax.persistence.*;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 2:12 AM
 */
@Table(name = "default_host", schema = "", catalog = "multishare")
@Entity
public class EntityDefaultHost implements Host {
    private Integer defaultHostId;

    @Column(name = "default_host_id")
    @Id
    @GeneratedValue
    public Integer getDefaultHostId() {
        return defaultHostId;
    }

    public void setDefaultHostId(Integer defaultHostId) {
        this.defaultHostId = defaultHostId;
    }

    private String username;

    @Column(name = "username")
    @Basic
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String password;

    @Column(name = "password")
    @Basic
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityDefaultHost that = (EntityDefaultHost) o;

        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (defaultHostId != null ? !defaultHostId.equals(that.defaultHostId) : that.defaultHostId != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = defaultHostId != null ? defaultHostId.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    private EntityFileHost fileHostByFileHostId;

    @ManyToOne
    public
    @JoinColumn(name = "file_host_id", referencedColumnName = "file_host_id", nullable = false)
    EntityFileHost getFileHostByFileHostId() {
        return fileHostByFileHostId;
    }

    public void setFileHostByFileHostId(EntityFileHost fileHostByFileHostId) {
        this.fileHostByFileHostId = fileHostByFileHostId;
    }
}
