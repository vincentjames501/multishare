package multishare.entities;

import javax.persistence.*;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 4:04 PM
 */
@Table(name = "user_roles", schema = "", catalog = "multishare")
@Entity
public class EntityUserRoles {
    private int userRoleId;

    @Column(name = "USER_ROLE_ID")
    @Id
    @GeneratedValue
    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    private String authority;

    @Column(name = "AUTHORITY")
    @Basic
    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityUserRoles that = (EntityUserRoles) o;

        if (userRoleId != that.userRoleId) return false;
        if (authority != null ? !authority.equals(that.authority) : that.authority != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userRoleId;
        result = 31 * result + (authority != null ? authority.hashCode() : 0);
        return result;
    }

    private EntityUsers usersByUserId;

    @ManyToOne
    public
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    EntityUsers getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(EntityUsers usersByUserId) {
        this.usersByUserId = usersByUserId;
    }
}
