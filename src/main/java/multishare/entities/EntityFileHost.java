package multishare.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:21 AM
 */
@Table(name = "file_host", schema = "", catalog = "multishare")
@Entity
public class EntityFileHost {
    private int fileHostId;

    @Column(name = "file_host_id")
    @Id
    public int getFileHostId() {
        return fileHostId;
    }

    public void setFileHostId(Integer fileHostId) {
        this.fileHostId = fileHostId;
    }

    public void setFileHostId(int fileHostId) {
        this.fileHostId = fileHostId;
    }

    private String name;

    @Column(name = "name")
    @Basic
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityFileHost that = (EntityFileHost) o;

        if (fileHostId != that.fileHostId) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fileHostId;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    private Collection<EntityUserHost> userHostsByFileHostId;

    @OneToMany(mappedBy = "fileHostByFileHostId")
    public Collection<EntityUserHost> getUserHostsByFileHostId() {
        return userHostsByFileHostId;
    }

    public void setUserHostsByFileHostId(Collection<EntityUserHost> userHostsByFileHostId) {
        this.userHostsByFileHostId = userHostsByFileHostId;
    }
}
