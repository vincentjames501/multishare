package multishare.entities;

import multishare.common.Host;

import javax.persistence.*;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 2:12 AM
 */
@javax.persistence.Table(name = "user_host", schema = "", catalog = "multishare")
@Entity
public class EntityUserHost implements Host {
    private Integer userHostId;

    @javax.persistence.Column(name = "user_host_id")
    @Id
    @GeneratedValue
    public Integer getUserHostId() {
        return userHostId;
    }

    public void setUserHostId(Integer userHostId) {
        this.userHostId = userHostId;
    }

    private String username;

    @javax.persistence.Column(name = "username")
    @Basic
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String password;

    @javax.persistence.Column(name = "password")
    @Basic
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityUserHost that = (EntityUserHost) o;

        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (userHostId != null ? !userHostId.equals(that.userHostId) : that.userHostId != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userHostId != null ? userHostId.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }

    private EntityUsers usersByUserId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    EntityUsers getUsersByUserId() {
        return usersByUserId;
    }

    public void setUsersByUserId(EntityUsers usersByUserId) {
        this.usersByUserId = usersByUserId;
    }

    private EntityFileHost fileHostByFileHostId;

    @ManyToOne
    @javax.persistence.JoinColumn(name = "file_host_id", referencedColumnName = "file_host_id", nullable = false)
    public EntityFileHost getFileHostByFileHostId() {
        return fileHostByFileHostId;
    }

    public void setFileHostByFileHostId(EntityFileHost fileHostByFileHostId) {
        this.fileHostByFileHostId = fileHostByFileHostId;
    }
}
