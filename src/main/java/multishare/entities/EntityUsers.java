package multishare.entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 4:04 PM
 */
@Table(name = "users", schema = "", catalog = "multishare")
@Entity
public class EntityUsers {
    private int userId;

    @Column(name = "USER_ID")
    @Id
    @GeneratedValue
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    private String username;

    @Column(name = "USERNAME")
    @Basic
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String password;

    @Column(name = "PASSWORD")
    @Basic
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private boolean enabled;

    @Column(name = "ENABLED")
    @Basic
    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
    
    private String email;

    @Column(name = "EMAIL")
    @Basic
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityUsers that = (EntityUsers) o;

        if (enabled != that.enabled) return false;
        if (userId != that.userId) return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (enabled ? 1 : 0);
        return result;
    }

    private Collection<EntityUserHost> userHostsByUserId;

    @OneToMany(mappedBy = "usersByUserId")
    public Collection<EntityUserHost> getUserHostsByUserId() {
        return userHostsByUserId;
    }

    public void setUserHostsByUserId(Collection<EntityUserHost> userHostsByUserId) {
        this.userHostsByUserId = userHostsByUserId;
    }

    private Collection<EntityUserRoles> userRolesesByUserId;

    @OneToMany(mappedBy = "usersByUserId", cascade=CascadeType.ALL)
    public Collection<EntityUserRoles> getUserRolesesByUserId() {
        return userRolesesByUserId;
    }

    public void setUserRolesesByUserId(Collection<EntityUserRoles> userRolesesByUserId) {
        this.userRolesesByUserId = userRolesesByUserId;
    }
}
