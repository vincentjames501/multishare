package multishare.entities;

import javax.persistence.*;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 9:34 PM
 */
@javax.persistence.Table(name = "uploaded_file_history", schema = "", catalog = "multishare")
@Entity
public class EntityUploadedFileHistory {
    private Integer uploadedFileHistoryId;

    @javax.persistence.Column(name = "uploaded_file_history_id")
    @Id
    @GeneratedValue
    public Integer getUploadedFileHistoryId() {
        return uploadedFileHistoryId;
    }

    public void setUploadedFileHistoryId(Integer uploadedFileHistoryId) {
        this.uploadedFileHistoryId = uploadedFileHistoryId;
    }

    private String status;

    @javax.persistence.Column(name = "status")
    @Basic
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntityUploadedFileHistory that = (EntityUploadedFileHistory) o;

        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (uploadedFileHistoryId != null ? !uploadedFileHistoryId.equals(that.uploadedFileHistoryId) : that.uploadedFileHistoryId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = uploadedFileHistoryId != null ? uploadedFileHistoryId.hashCode() : 0;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    private EntityFileHost fileHostByFileHostId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "file_host_id", referencedColumnName = "file_host_id", nullable = false)
    EntityFileHost getFileHostByFileHostId() {
        return fileHostByFileHostId;
    }

    public void setFileHostByFileHostId(EntityFileHost fileHostByFileHostId) {
        this.fileHostByFileHostId = fileHostByFileHostId;
    }

    private EntityUploadedFile uploadedFileByUploadedFileId;

    @ManyToOne
    public
    @javax.persistence.JoinColumn(name = "uploaded_file_id", referencedColumnName = "uploaded_file_id", nullable = false)
    EntityUploadedFile getUploadedFileByUploadedFileId() {
        return uploadedFileByUploadedFileId;
    }

    public void setUploadedFileByUploadedFileId(EntityUploadedFile uploadedFileByUploadedFileId) {
        this.uploadedFileByUploadedFileId = uploadedFileByUploadedFileId;
    }
}
