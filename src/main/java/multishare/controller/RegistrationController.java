package multishare.controller;

import multishare.service.adapter.UserServiceAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: VINCENT
 * Date: 1/3/12
 * Time: 4:27 PM
 */
@Controller
public class RegistrationController {
    @Resource
    private UserServiceAdapter userServiceAdapter;

    @RequestMapping(value = "/register.htm", method = RequestMethod.GET)
    public ModelAndView registerRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("register");
    }

    @RequestMapping(value = "/register.htm", method = RequestMethod.POST)
    public ModelAndView registerPostRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView registrationModelAndView = new ModelAndView("register");
        String username = httpServletRequest.getParameter("username");
        String password = httpServletRequest.getParameter("password");
        String email = httpServletRequest.getParameter("mail");

        if(userServiceAdapter.userExists(username)) {
            registrationModelAndView.addObject("errorMessage", "Error: Username already exists");
            return registrationModelAndView;
        } else if(userServiceAdapter.emailAlreadyExists(email)) {
            registrationModelAndView.addObject("errorMessage", "Error: Email already in use");
            return registrationModelAndView;
        }
        userServiceAdapter.createNewUser(username, password, email);
        registrationModelAndView.addObject("successMessage", "You've been successfully added!  You can now log in.");

        return registrationModelAndView;
    }
}
