package multishare.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 5:28 PM
 */
@Controller
public class MainController {
    @RequestMapping(value = "/index.htm")
    public ModelAndView indexRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("home");
    }

    @RequestMapping(value = "/contact.htm")
    public ModelAndView contactRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("contact");
    }

    @RequestMapping(value = "/dmca.htm")
    public ModelAndView dmcaRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("dmca");
    }

    @RequestMapping(value = "/errorPage.htm")
    public ModelAndView errorRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("errorPage");
    }

    @RequestMapping(value = "/upload.htm")
    public ModelAndView uploadRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("upload");
    }
}
