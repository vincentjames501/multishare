package multishare.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: VINCENT
 * Date: 12/26/11
 * Time: 9:55 PM
 */
@Controller
public class AccountController {
    @RequestMapping(value = "/account.htm")
    public ModelAndView accountRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView accountModel = new ModelAndView("account");
        return accountModel;
    }
}
