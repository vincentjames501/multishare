package multishare.controller;

import multishare.service.adapter.LinkAccountsServiceAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by IntelliJ IDEA.
 * User: Penguin
 * Date: 12/28/11
 * Time: 3:29 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class LinkAccountsController {
    @Resource
    private LinkAccountsServiceAdapter linkAccountsServiceAdapter;

    @RequestMapping(value = "/linkAccounts.htm")
    public ModelAndView linkAccountsRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        return new ModelAndView("linkAccounts");
    }
    
    @RequestMapping(value = "/updateAccountInformation.htm", method = RequestMethod.POST)
    public ModelAndView updateAccountInformationRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        if(httpServletRequest.getUserPrincipal()==null){
            return new ModelAndView("json");
        }
        String user = httpServletRequest.getUserPrincipal().getName();
        String username = httpServletRequest.getParameter("username");
        String password = httpServletRequest.getParameter("password");
        String host = httpServletRequest.getParameter("host");
        linkAccountsServiceAdapter.updateAccountInformation(username, password, host, user);
        return new ModelAndView("json");
    }
}
