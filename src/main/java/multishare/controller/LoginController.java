package multishare.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: VINCENT
 * Date: 12/26/11
 * Time: 3:05 PM
 */
@Controller
public class LoginController {
    @RequestMapping(value = "/login.htm")
    public ModelAndView indexRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("login");
    }
}
