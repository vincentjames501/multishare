package multishare.controller;

import multishare.email.EmailClient;
import multishare.entities.EntityUsers;
import multishare.service.adapter.UserServiceAdapter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * User: VINCENT
 * Date: 1/4/12
 * Time: 10:14 AM
 */
@Controller
public class ForgotPasswordController{
    @Resource
    private UserServiceAdapter userServiceAdapter;

    @RequestMapping(value = "/forgotPassword.htm", method = RequestMethod.GET)
    public ModelAndView forgotPasswordRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("forgotPassword");
    }

    @RequestMapping(value = "/forgotPassword.htm", method = RequestMethod.POST)
    public ModelAndView forgotPasswordPostRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        ModelAndView forgotPasswordView = new ModelAndView("forgotPassword");
        String username = httpServletRequest.getParameter("username");
        String email = httpServletRequest.getParameter("mail");

        if(username.isEmpty() && email.isEmpty()) {
            forgotPasswordView.addObject("errorMessage", "Error: You did not fill in any fields");
        }
        if(!username.isEmpty() && !userServiceAdapter.userExists(username)) {
            forgotPasswordView.addObject("errorMessage", "Error: Username does not exist");
            return forgotPasswordView;
        } else if(!username.isEmpty()) {
            EntityUsers user = userServiceAdapter.getUserByName(username);
            String userEmail = user.getEmail();
            new EmailClient().sendEmail("MultiShare - Password Recovery", buildPasswordEmailContent(user), userEmail);
        } else if(!email.isEmpty() && !userServiceAdapter.emailAlreadyExists(email)) {
            forgotPasswordView.addObject("errorMessage", "Error: Email does not exist");
            return forgotPasswordView;
        } else if(!email.isEmpty()) {
            EntityUsers user = userServiceAdapter.getUserByEmail(email);
            new EmailClient().sendEmail("MultiShare - Password Recovery", buildPasswordEmailContent(user), email);
        }
        forgotPasswordView.addObject("successMessage", "An e-mail has been sent, please check your e-mail.");
        return forgotPasswordView;
    }

    private String buildPasswordEmailContent(EntityUsers user) {
        return "---------------------------------------------------\n" +
                "Your requested information from MultiShare.\n" +
                "---------------------------------------------------\n" +
                "\n" +
                "Your username: " + user.getUsername() + "\n" +
                "Your password: " + user.getPassword() + "\n";
    }
}
