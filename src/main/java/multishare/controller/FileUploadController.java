package multishare.controller;

import multishare.common.FileTransfer;
import multishare.common.Host;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPluginHelper;
import multishare.entities.EntityDefaultHost;
import multishare.entities.EntityUploadedFile;
import multishare.entities.EntityUserHost;
import multishare.service.FileTransferService;
import multishare.service.adapter.FileHostServiceAdapter;
import multishare.service.adapter.UploadedFileServiceAdapter;
import multishare.service.plugins.download.DownloadHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * User: VINCENT
 * Date: 12/26/11
 * Time: 4:33 PM
 */
@Controller
public class FileUploadController {
    @Resource
    private UploadPluginHelper uploadPluginHelper;

    @Resource
    private FileTransferService fileTransferService;

    @Resource
    private FileHostServiceAdapter fileHostServiceAdapter;

    @Resource
    private UploadedFileServiceAdapter uploadedFileServiceAdapter;

    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    @RequestMapping(value = "/singleFileUpload.htm")
    public ModelAndView singleFileUploadRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("singleFileUpload");
    }

    @RequestMapping(value = "/multiFileUpload.htm")
    public ModelAndView multiFileUploadRequestHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        return new ModelAndView("multiFileUpload");
    }

    @RequestMapping(value = "/singleFileUpload.htm", method = RequestMethod.POST)
    public ModelAndView singleFileUploadHandler(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, @RequestParam("file") MultipartFile file) throws Exception {
        ModelAndView jsonView = new ModelAndView("json");
        String userName = null;
        if (httpServletRequest.getUserPrincipal() != null) {
            userName = httpServletRequest.getUserPrincipal().getName();
        }
        String fileName = httpServletRequest.getParameter("name");
        String UUID = httpServletRequest.getParameter("UUID");
        Long totalSize = Long.parseLong(httpServletRequest.getParameter("size"));
        String[] fileHosts = httpServletRequest.getParameter("fileHosts").split(",");
        if(noFileHostsSelected(fileHosts)) {
            return jsonView;
        }
        createUploadDirectoryIfNotExist();
        File tempUUIDDirectory = createTempUUIDDirectory(UUID);
        File uploadFile = new File("uploads" + FILE_SEPARATOR + UUID + FILE_SEPARATOR + fileName);
        new DownloadHelper().downloadFile(uploadFile, file.getInputStream(), new FileTransfer(), file.getSize());
        if(uploadFile.length()!=totalSize) {
            return jsonView;
        }
        EntityUploadedFile uploadedFile = null;
        if (userName != null) {
            uploadedFile = uploadedFileServiceAdapter.createNewUploadedFile(userName, fileName, fileHosts);
        } else {
            uploadedFile = uploadedFileServiceAdapter.createNewUploadedFile("DEFAULT_USER", fileName, fileHosts);
        }
        for (String fileHost : fileHosts) {
            Host host;
            if (userName == null) {
                host = fileHostServiceAdapter.getDefaultFileHostDetailsByName(fileHost);
            } else {
                host = fileHostServiceAdapter.getUserFileHostDetailsByName(userName, fileHost);
            }
            UploadFileTransfer fileTransfer = new UploadFileTransfer(uploadFile, host);
            fileTransferService.queueFileTransfer(fileTransfer, uploadedFile);
        }
        return jsonView;
    }

    private boolean noFileHostsSelected(String[] fileHosts) {
        return fileHosts.length==1 && fileHosts[0].isEmpty();
    }

    private void deleteUploadedFile(File tempUUIDDirectory, File uploadFile) {
        uploadFile.delete();
        tempUUIDDirectory.delete();
    }

    private File createTempUUIDDirectory(String uuid) {
        File tempUploadDirectory = new File("uploads" + FILE_SEPARATOR + uuid);
        if (!tempUploadDirectory.exists()) {
            tempUploadDirectory.mkdir();
        }
        return tempUploadDirectory;
    }

    private void createUploadDirectoryIfNotExist() {
        File uploadsDirectory = new File("uploads");
        if (!uploadsDirectory.exists()) {
            uploadsDirectory.mkdir();
        }
    }
}
