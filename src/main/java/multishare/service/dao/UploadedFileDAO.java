package multishare.service.dao;

import multishare.entities.EntityUploadedFile;
import multishare.entities.EntityUploadedFileHistory;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 9:35 PM
 */
public class UploadedFileDAO {
    @Resource
    private SessionFactory sessionFactory;

    @Transactional
    public void save(EntityUploadedFile uploadedFile) {
        sessionFactory.getCurrentSession().save(uploadedFile);
    }

    @Transactional
    public void updateFileHistory(EntityUploadedFileHistory entityUploadedFileHistory) {
        sessionFactory.getCurrentSession().save(entityUploadedFileHistory);
    }
}
