package multishare.service.dao;

import multishare.entities.EntityDefaultHost;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import multishare.entities.EntityFileHost;
import multishare.entities.EntityUserHost;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:16 AM
 */
public class FileHostDAO {
    @Resource
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Transactional
    public List<EntityFileHost> getAllFileHosts() {
        return sessionFactory.getCurrentSession().createCriteria(EntityFileHost.class).list();
    }

    @SuppressWarnings("unchecked")
    @Transactional
    public List<EntityUserHost> getFileHostsByUser(String userName) {
        return sessionFactory.getCurrentSession().createCriteria(EntityUserHost.class).createAlias("usersByUserId", "u").add(Restrictions.eq("u.username", userName)).list();
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public EntityFileHost getHostByName(String host) {
        return (EntityFileHost)sessionFactory.getCurrentSession().createCriteria(EntityFileHost.class).add(Restrictions.eq("name", host)).uniqueResult();
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public EntityUserHost getUserFileHostDetailsByName(String userName, String host) {
        return (EntityUserHost)sessionFactory.getCurrentSession().createCriteria(EntityUserHost.class).createAlias("usersByUserId", "u").createAlias("fileHostByFileHostId", "f").add(Restrictions.eq("u.username", userName)).add(Restrictions.eq("f.name", host)).uniqueResult();
    }

    @Transactional
    @SuppressWarnings("unchecked")
    public EntityDefaultHost getDefaultFileHostDetailsByName(String host) {
        return (EntityDefaultHost)sessionFactory.getCurrentSession().createCriteria(EntityDefaultHost.class).createAlias("fileHostByFileHostId", "f").add(Restrictions.eq("f.name", host)).uniqueResult();
    }
}
