package multishare.service.dao;

import multishare.entities.EntityUserHost;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 * User: Penguin
 * Date: 12/28/11
 * Time: 3:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class LinkAccountsDAO {
    @Resource
    private SessionFactory sessionFactory;

    @Resource
    private FileHostDAO fileHostDAO;

    @Resource
    private UserDAO userDAO;

    @Transactional
    public void updateAccountInformation(EntityUserHost userHost, String username, String password) {
        userHost.setUsername(username);
        userHost.setPassword(password);
        sessionFactory.getCurrentSession().saveOrUpdate(userHost);
    }

    @Transactional
    public void addUserHost(String username, String password, String host, String user) {
        EntityUserHost userHost = new EntityUserHost();
        userHost.setFileHostByFileHostId(fileHostDAO.getHostByName(host));
        userHost.setUsersByUserId(userDAO.getUserByName(user));
        updateAccountInformation(userHost,username,password);
    }
}
