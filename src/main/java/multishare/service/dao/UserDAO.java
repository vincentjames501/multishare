package multishare.service.dao;

import multishare.entities.EntityUserRoles;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;
import multishare.entities.EntityUsers;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 4:35 PM
 */
public class UserDAO {
    @Resource
    private SessionFactory sessionFactory;
    
    @SuppressWarnings("unchecked")
    @Transactional
    public List<EntityUsers> getAllUsers() {
        return sessionFactory.getCurrentSession().createCriteria(EntityUsers.class).list();
    }

    @Transactional
    public EntityUsers getUserByName(String userName) {
        return (EntityUsers)sessionFactory.getCurrentSession().createCriteria(EntityUsers.class).add(Restrictions.eq("username", userName)).uniqueResult();
    }

    @Transactional
    public EntityUsers getUserByEmail(String email) {
        return (EntityUsers)sessionFactory.getCurrentSession().createCriteria(EntityUsers.class).add(Restrictions.eq("email", email)).uniqueResult();
    }

    @Transactional
    public void createNewUser(String username, String password, String email) {
        EntityUsers user = new EntityUsers();
        user.setEmail(email);
        user.setEnabled(true);
        user.setPassword(password);
        user.setUsername(username);
        EntityUserRoles userRole = new EntityUserRoles();
        userRole.setAuthority("ROLE_USER");
        userRole.setUsersByUserId(user);
        user.setUserRolesesByUserId(Arrays.asList(userRole));
        sessionFactory.getCurrentSession().save(user);
    }
}
