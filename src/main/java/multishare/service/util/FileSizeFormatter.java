package multishare.service.util;

/**
 * User: VINCENT
 * Date: 12/26/11
 * Time: 3:09 PM
 */
public class FileSizeFormatter {
    public static String formatFileLength(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < unit) return bytes + " B";
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "i");
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
    }
}
