package multishare.service.adapter;

import multishare.entities.EntityDefaultHost;
import multishare.entities.EntityFileHost;
import multishare.entities.EntityUserHost;
import multishare.service.dao.FileHostDAO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:15 AM
 */
public class FileHostServiceAdapter {
    @Resource 
    private FileHostDAO fileHostDAO;

    public List<String> getFileHostsByUserName(String userName) {
        List<EntityUserHost> fileHosts = fileHostDAO.getFileHostsByUser(userName);
        List<String> fileHostNames = new ArrayList<String>();
        for (EntityUserHost fileHost : fileHosts) {
            fileHostNames.add(fileHost.getFileHostByFileHostId().getName());
        }
        return fileHostNames;
    }

    public List<String> getAllFileHosts() {
        List<EntityFileHost> fileHosts = fileHostDAO.getAllFileHosts();
        List<String> fileHostNames = new ArrayList<String>();
        for (EntityFileHost fileHost : fileHosts) {
            fileHostNames.add(fileHost.getName());
        }
        return fileHostNames;
    }
    
    public EntityUserHost getUserFileHostDetailsByName(String userName, String host) {
        return fileHostDAO.getUserFileHostDetailsByName(userName, host);
    }

    public EntityDefaultHost getDefaultFileHostDetailsByName(String host) {
        return fileHostDAO.getDefaultFileHostDetailsByName(host);
    }

    public EntityFileHost getHostByName(String host) {
        return fileHostDAO.getHostByName(host);
    }

    public void setFileHostDAO(FileHostDAO fileHostDAO) {
        this.fileHostDAO = fileHostDAO;
    }
}
