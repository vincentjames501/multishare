package multishare.service.adapter;

import multishare.entities.EntityUploadedFile;
import multishare.entities.EntityUploadedFileHistory;
import multishare.entities.EntityUsers;
import multishare.service.dao.UploadedFileDAO;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 9:35 PM
 */
public class UploadedFileServiceAdapter {
    @Resource
    private UploadedFileDAO uploadedFileDAO;

    @Resource
    private FileHostServiceAdapter fileHostServiceAdapter;

    @Resource
    private UserServiceAdapter userServiceAdapter;

    public EntityUploadedFile createNewUploadedFile(String userName, String fileName, String[] fileHosts) {
        EntityUsers user = userServiceAdapter.getUserByName(userName);

        EntityUploadedFile uploadedFile = new EntityUploadedFile();

        List<EntityUploadedFileHistory> fileStatuses = new ArrayList<EntityUploadedFileHistory>();
        for (String fileHost : fileHosts) {
            EntityUploadedFileHistory fileStatus = new EntityUploadedFileHistory();
            fileStatus.setUploadedFileByUploadedFileId(uploadedFile);
            fileStatus.setFileHostByFileHostId(fileHostServiceAdapter.getHostByName(fileHost));
            fileStatus.setStatus(null);
            fileStatuses.add(fileStatus);
        }

        uploadedFile.setName(fileName);
        uploadedFile.setUser(user);
        uploadedFile.setUploadedFileHistoriesByUploadedFileId(fileStatuses);
        uploadedFileDAO.save(uploadedFile);
        return uploadedFile;
    }

    public void updateStatus(EntityUploadedFileHistory entityUploadedFileHistory, String message) {
        entityUploadedFileHistory.setStatus(message);
        uploadedFileDAO.updateFileHistory(entityUploadedFileHistory);
    }
}
