package multishare.service.adapter;

import multishare.entities.EntityUsers;
import multishare.service.dao.UserDAO;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 4:45 PM
 */
public class UserServiceAdapter {
    @Resource
    private UserDAO userDAO;

    public List<EntityUsers> getAllUsers() {
        return userDAO.getAllUsers();
    }

    public EntityUsers getUserByName(String userName) {
        return userDAO.getUserByName(userName);
    }

    public boolean emailAlreadyExists(String email) {
        return userDAO.getUserByEmail(email)!=null;
    }

    public void createNewUser(String username, String password, String email) {
        userDAO.createNewUser(username, password, email);
    }

    public boolean userExists(String username) {
        return getUserByName(username)!=null;
    }

    public EntityUsers getUserByEmail(String email) {
        return userDAO.getUserByEmail(email);
    }
}
