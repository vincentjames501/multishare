package multishare.service.adapter;

import multishare.entities.EntityUserHost;
import multishare.service.dao.FileHostDAO;
import multishare.service.dao.LinkAccountsDAO;

import javax.annotation.Resource;

/**
 * Created by IntelliJ IDEA.
 * User: Penguin
 * Date: 12/28/11
 * Time: 3:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class LinkAccountsServiceAdapter {
    @Resource
    private LinkAccountsDAO linkAccountsDAO;

    @Resource
    private FileHostDAO fileHostDAO;

    public void updateAccountInformation(String username, String password, String host, String user) {
        EntityUserHost userHost = fileHostDAO.getUserFileHostDetailsByName(user, host);
        if (userHost != null) {
            linkAccountsDAO.updateAccountInformation(userHost, username, password);
        } else {
            linkAccountsDAO.addUserHost(username, password, host, user);
        }
    }
}
