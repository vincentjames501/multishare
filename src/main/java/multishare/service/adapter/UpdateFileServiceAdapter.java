package multishare.service.adapter;

import org.apache.log4j.Logger;

import java.io.File;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 12:09 AM
 */
public class UpdateFileServiceAdapter {
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");
    private static final String DELETE_OPERATION = "del";
    private static final Logger LOGGER = Logger.getLogger(UpdateFileServiceAdapter.class);
    private static final String EDIT_OPERATION = "edit";

    public void update(String userName, String operation, String[] files, String newName) {
        if(operation.equalsIgnoreCase(DELETE_OPERATION)) {
            deleteFiles(userName, files);
        } else if(operation.equalsIgnoreCase(EDIT_OPERATION)) {
            renameFile(userName, files[0], newName);    
        }
    }

    private void renameFile(String userDirectory, String oldName, String newName) {
        File fileToRename = new File(userDirectory + FILE_SEPARATOR + oldName);
        File renamedFile = new File(userDirectory + FILE_SEPARATOR + newName);
        if(fileToRename.exists()) {
            boolean successfullyRenamed = fileToRename.renameTo(renamedFile);
            if(!successfullyRenamed) {
                LOGGER.warn(oldName + " was not able to be renamed!");
            }
        }
    }

    private void deleteFiles(String userName, String[] files) {
        for (String file : files) {
            File fileToDelete = new File(userName + FILE_SEPARATOR + file);
            if(fileToDelete.exists()) {
                boolean fileDeleted = fileToDelete.delete();
                if(!fileDeleted) {
                    LOGGER.warn(file + " was not successfully deleted");
                }
            }
        }
    }
}
