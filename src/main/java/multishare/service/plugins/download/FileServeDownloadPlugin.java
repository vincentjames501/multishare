package multishare.service.plugins.download;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import multishare.common.DownloadPlugin;
import multishare.common.FileTransfer;

import java.io.File;
import java.io.InputStream;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:22 PM
 */
public class FileServeDownloadPlugin extends DownloadHelper implements DownloadPlugin  {
    @Override
    public void start(FileTransfer fileTransfer) throws Exception {
        String username = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient httpClient = new HttpClient();

        GetMethod fileServeDownloadMethod = new GetMethod(fileTransfer.getURL());
        addBasicRequestHeaders(username, password, fileServeDownloadMethod);
        httpClient.executeMethod(fileServeDownloadMethod);

        String fileName = fileServeDownloadMethod.getResponseHeader("Content-Disposition").getValue().substring(30);

        File saveFileTo = new File(fileTransfer.getFileToTransfer().getAbsolutePath() + System.getProperty("file.separator") + fileName);

        InputStream inputStream = fileServeDownloadMethod.getResponseBodyAsStream();
        Long contentLength = Long.parseLong(fileServeDownloadMethod.getResponseHeader("Content-Length").getValue());
        downloadFile(saveFileTo, inputStream, fileTransfer, contentLength);
    }
}
