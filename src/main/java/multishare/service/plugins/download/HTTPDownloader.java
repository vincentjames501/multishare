package multishare.service.plugins.download;

import multishare.common.DownloadPlugin;
import multishare.common.FileTransfer;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * User: VINCENT
 * Date: 1/4/12
 * Time: 6:17 PM
 */
public class HTTPDownloader extends DownloadHelper implements DownloadPlugin {
    @Override
    public void start(FileTransfer fileTransfer) throws Exception {
        if(fileTransfer.getURL().toLowerCase().contains("ftp://")) {
            startFTPFileTransfer(fileTransfer);
        } else if (!fileTransfer.getURL().toLowerCase().contains("http://")) {
            throw new Exception("Unrecognized link");
        } else if(fileTransfer.getURL().contains("@")) {
            startAuthenticatedFileTransfer(fileTransfer);
        } else {
            startNormalFileTransfer(fileTransfer);
        }
    }

    private void startNormalFileTransfer(FileTransfer fileTransfer) throws Exception {
        HttpClient httpClient = new HttpClient();
        GetMethod authenticatedDownloadMethod = new GetMethod(formatURL(fileTransfer));
        addBasicRequestHeaders(authenticatedDownloadMethod);
        httpClient.executeMethod(authenticatedDownloadMethod);

        String fileName = URLDecoder.decode(authenticatedDownloadMethod.getPath().substring(authenticatedDownloadMethod.getPath().lastIndexOf("/") + "/".length()), "UTF-8");
        File saveFileTo = new File(UUID.randomUUID().toString() + System.getProperty("file.separator") + fileName);
        InputStream inputStream = authenticatedDownloadMethod.getResponseBodyAsStream();
        Long contentLength = Long.parseLong(authenticatedDownloadMethod.getResponseHeader("Content-Length").getValue());
        downloadFile(saveFileTo, inputStream, fileTransfer, contentLength);
    }

    private String formatURL(FileTransfer fileTransfer) throws Exception {
        String url = fileTransfer.getURL();
        String escapedFileName = URLEncoder.encode(url.substring(url.lastIndexOf("/")+"/".length()), "UTF-8");
        url = url.substring(0, url.lastIndexOf("/")+"/".length()) + escapedFileName;
        return url;
    }

    private void startAuthenticatedFileTransfer(FileTransfer fileTransfer) throws Exception {
        HttpClient httpClient = new HttpClient();
        String username = fileTransfer.getURL().substring(fileTransfer.getURL().indexOf("http://")+"http://".length(), fileTransfer.getURL().lastIndexOf(":"));
        String password = fileTransfer.getURL().substring(fileTransfer.getURL().lastIndexOf(":")+":".length(), fileTransfer.getURL().indexOf("@"));
        GetMethod authenticatedDownloadMethod = new GetMethod(stripLoginFromURL(fileTransfer));
        addBasicRequestHeaders(username, password, authenticatedDownloadMethod);
        httpClient.executeMethod(authenticatedDownloadMethod);
        
        String fileName = URLDecoder.decode(authenticatedDownloadMethod.getPath().substring(authenticatedDownloadMethod.getPath().lastIndexOf("/") + "/".length()), "UTF-8");
        File saveFileTo = new File(UUID.randomUUID().toString() + System.getProperty("file.separator") + fileName);
        InputStream inputStream = authenticatedDownloadMethod.getResponseBodyAsStream();
        Long contentLength = Long.parseLong(authenticatedDownloadMethod.getResponseHeader("Content-Length").getValue());
        downloadFile(saveFileTo, inputStream, fileTransfer, contentLength);
    }

    private String stripLoginFromURL(FileTransfer fileTransfer) throws Exception {
        String url = "http://"+fileTransfer.getURL().substring(fileTransfer.getURL().indexOf("@")+"@".length());
        String escapedFileName = URLEncoder.encode(url.substring(url.lastIndexOf("/")+"/".length()), "UTF-8");
        url = url.substring(0, url.lastIndexOf("/")+"/".length()) + escapedFileName;
        return url;
    }

    private void startFTPFileTransfer(FileTransfer fileTransfer) {

    }
}
