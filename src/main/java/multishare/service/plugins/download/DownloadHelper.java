package multishare.service.plugins.download;

import multishare.service.util.FileSizeFormatter;
import org.apache.commons.httpclient.methods.GetMethod;
import multishare.common.FileTransfer;
import sun.misc.BASE64Encoder;

import java.io.*;

/**
 * User: VINCENT
 * Date: 12/25/11
 * Time: 2:12 PM
 */
public class DownloadHelper {
    private long previousTime = System.currentTimeMillis();
    private long previousBytes = 0;

    public void downloadFile(File saveFileTo, InputStream inputStream, FileTransfer fileTransfer, Long contentLength) {
        try {
            OutputStream fileOutputStream = new FileOutputStream(saveFileTo, true);
            byte buf[] = new byte[2048];
            int readLength = 0;
            long totalReadLength = 0;
            while ((readLength = inputStream.read(buf)) > 0) {
                totalReadLength += readLength;

                long newTime = System.currentTimeMillis();
                if ((newTime - previousTime) / 1000.0f > 1.0f) {
                    fileTransfer.setSpeed(FileSizeFormatter.formatFileLength((long) ((totalReadLength - previousBytes) / (((newTime - previousTime) / 1000.0f))), false) + "/s");
                    previousBytes = totalReadLength;
                    previousTime = newTime;
                }

                fileTransfer.setProgress(((float) totalReadLength) / ((float) contentLength));
                fileOutputStream.write(buf, 0, readLength);
            }
            fileOutputStream.close();
            inputStream.close();
            fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
        } catch (IOException e) {

        }
    }

    protected void addBasicRequestHeaders(String username, String password, GetMethod downloadMethod) {
        downloadMethod.setRequestHeader("Authorization", "Basic " + getAuthorization(username, password));
        downloadMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        downloadMethod.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        downloadMethod.setRequestHeader("Accept-Language", "en-us,en;q=0.5");
        downloadMethod.setRequestHeader("Accept-Encoding", "gzip, deflate");
        downloadMethod.setRequestHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        downloadMethod.setRequestHeader("Connection", "keep-alive");
    }

    protected void addBasicRequestHeaders(GetMethod downloadMethod) {
        downloadMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        downloadMethod.setRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        downloadMethod.setRequestHeader("Accept-Language", "en-us,en;q=0.5");
        downloadMethod.setRequestHeader("Accept-Encoding", "gzip, deflate");
        downloadMethod.setRequestHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        downloadMethod.setRequestHeader("Connection", "keep-alive");
    }

    private String getAuthorization(String username, String password) {
        BASE64Encoder enc = new BASE64Encoder();
        String logonToken = username + ":" + password;
        return enc.encode(logonToken.getBytes());
    }
}
