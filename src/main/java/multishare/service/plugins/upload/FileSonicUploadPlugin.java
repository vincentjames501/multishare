package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import multishare.service.plugins.upload.api.FSApi;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 6:56 PM
 */
public class FileSonicUploadPlugin extends UploadHelper implements UploadPlugin {
    private FSApi fsApi = new FSApi();

    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        return fsApi.start(fileTransfer, "api.filesonic.com", "FileSonic");
    }
}
