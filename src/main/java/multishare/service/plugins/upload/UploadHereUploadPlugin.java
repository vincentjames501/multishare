package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

/**
 * User: VINCENT
 * Date: 1/3/12
 * Time: 9:16 AM
 */
public class UploadHereUploadPlugin extends UploadHelper implements UploadPlugin {
    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        String userName = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient httpClient = new HttpClient();
        httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        PostMethod loginPostMethod = new PostMethod("http://uploadhere.com/login");
        loginPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        loginPostMethod.setParameter("do", "login");
        loginPostMethod.setParameter("username", userName);
        loginPostMethod.setParameter("password", password);
        httpClient.executeMethod(loginPostMethod);


        GetMethod getLoginPage = new GetMethod("http://uploadhere.com");
        httpClient.executeMethod(getLoginPage);
        
        String uploadURL = getActionURL(getLoginPage.getResponseBodyAsString());
        return "Successfully Uploaded To UploadHere - Download Link Available At " + uploadFile(fileTransfer, uploadURL, httpClient);
    }

    private String getActionURL(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("action=\"http://www") + "action=\"http://www".length();
        return "http://www" + uploadPageResponse.substring(uploadSuffixPosition,
                uploadPageResponse.indexOf("\"", uploadSuffixPosition));
    }

    private String uploadFile(FileTransfer fileTransfer, String actionURL, HttpClient httpClient) throws Exception {
        PostMethod uploadDataPostMethod = new PostMethod(actionURL);
        Part uploadIdentifierPart = buildStringPart("UPLOAD_IDENTIFIER", stripUploadIdentifier(actionURL));
        Part userPart = buildStringPart("u", getUserToken(httpClient));
        FilePart file = new FilePart("file_0", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{uploadIdentifierPart, userPart, file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);

        String uploadResponse = uploadDataPostMethod.getResponseBodyAsString();

        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
        return getFinalUploadURL(uploadResponse);
    }

    private String stripUploadIdentifier(String actionURL) {
        return actionURL.substring(actionURL.indexOf("UPLOAD_IDENTIFIER") + "UPLOAD_IDENTIFIER".length());
    }

    private String getUserToken(HttpClient httpClient) {
        for (Cookie cookie : httpClient.getState().getCookies()) {
            if(cookie.getName().equalsIgnoreCase("u"))  {
                return cookie.getValue();
            }
        }
        return "";
    }

    private String getFinalUploadURL(String uploadResponse) {
        int beginningIndex = uploadResponse.indexOf("downloadid\":\"")+"downloadid\":\"".length();
        return "http://uploadhere.com/" + uploadResponse.substring(beginningIndex, uploadResponse.indexOf("\"", beginningIndex));
    }
}
