package multishare.service.plugins.upload.api;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.service.plugins.upload.CountingMultipartRequestEntity;
import multishare.service.plugins.upload.ProgressListener;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 10:09 AM
 */
public class FSApi {
    public String start(UploadFileTransfer fileTransfer, String domain, String host) throws Exception {
        String username = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient fileSonicUploadClient = new HttpClient();
        GetMethod getUploadURLMethod = new GetMethod("http://"+domain+"/upload?method=getUploadUrl&u=" + username + "&p=" + password);
        fileSonicUploadClient.executeMethod(getUploadURLMethod);
        String jsonResponseString = getUploadURLMethod.getResponseBodyAsString();

        String status = (String) ((JSONObject)((JSONObject)new JSONObject(jsonResponseString).get("FSApi_Upload")).get("getUploadUrl")).get("status");
        if(!status.equalsIgnoreCase("success")) {
            return "Error: wrong username/password?";
        }
        String uploadURL = (String) ((JSONObject)((JSONObject)((JSONObject)new JSONObject(jsonResponseString).get("FSApi_Upload")).get("getUploadUrl")).get("response")).get("url");
        Integer maxFileSize = (Integer) ((JSONObject)((JSONObject)((JSONObject)new JSONObject(jsonResponseString).get("FSApi_Upload")).get("getUploadUrl")).get("response")).get("max-filesize");
        if(fileTransfer.getFileToTransfer().length()/1024>maxFileSize*1024) {
            return "Error: filesize is too large!";
        }

        PostMethod uploadDataPostMethod = new PostMethod(uploadURL);
        FilePart file = new FilePart("files[]", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        fileSonicUploadClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        fileSonicUploadClient.executeMethod(uploadDataPostMethod);
        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);


        String json = uploadDataPostMethod.getResponseBodyAsString();

        status = (String) ((JSONObject)((JSONObject)new JSONObject(json).get("FSApi_Upload")).get("postFile")).get("status");
        if(!status.equalsIgnoreCase("success")) {
            return "Error: file upload did not complete successfully";
        }
        String uploadedURL = (String) ((JSONObject)((JSONArray)((JSONObject)((JSONObject)((JSONObject)new JSONObject(json).get("FSApi_Upload")).get("postFile")).get("response")).get("files")).get(0)).get("url");
        String name = (String) ((JSONObject)((JSONArray)((JSONObject)((JSONObject)((JSONObject)new JSONObject(json).get("FSApi_Upload")).get("postFile")).get("response")).get("files")).get(0)).get("name");
        return "Successfully Uploaded To " + host + " - Download Link Available At " + uploadedURL + "/" + name;
    }
}
