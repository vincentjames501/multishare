package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 12:21 PM
 */
public class HotFileUploadPlugin extends UploadHelper implements UploadPlugin {
    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        String username = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient httpClient = new HttpClient();
        httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        PostMethod hotfileLoginMethod = new PostMethod("http://hotfile.com/login.php");
        hotfileLoginMethod.setRequestHeader("Host", "hotfile.com");
        hotfileLoginMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        hotfileLoginMethod.setRequestHeader("Referer", "http://hotfile.com/");
        hotfileLoginMethod.addParameter("returnto", "/");
        hotfileLoginMethod.addParameter("user", username);
        hotfileLoginMethod.addParameter("pass", password);
        httpClient.executeMethod(hotfileLoginMethod);

        GetMethod cookieCheckMethod = new GetMethod("http://hotfile.com/?cookiecheck=1");
        cookieCheckMethod.setRequestHeader("Host", "hotfile.com");
        cookieCheckMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        cookieCheckMethod.setRequestHeader("Referer", "http://hotfile.com/");
        httpClient.executeMethod(cookieCheckMethod);

        String uploadPageResponse = cookieCheckMethod.getResponseBodyAsString();
        String actionURL = getActionURL(uploadPageResponse);

        return "Successfully Uploaded To HotFile - Download Link Available At " + uploadFile(fileTransfer, actionURL, httpClient);
    }

    private String uploadFile(FileTransfer fileTransfer, String actionURL, HttpClient httpClient) throws Exception {
        PostMethod uploadDataPostMethod = new PostMethod(actionURL);
        uploadDataPostMethod.addRequestHeader("Connection", "keep-alive");
        uploadDataPostMethod.addRequestHeader("Cache-Control", "max-age=0");
        uploadDataPostMethod.addRequestHeader("Origin", "http://hotfile.com");
        uploadDataPostMethod.addRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        uploadDataPostMethod.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        uploadDataPostMethod.addRequestHeader("Accept-Language", "en-US,en;q=0.8");
        uploadDataPostMethod.addRequestHeader("Accept-Charset", "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3");
        uploadDataPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        uploadDataPostMethod.setRequestHeader("Referer", "http://hotfile.com");
        FilePart file = new FilePart("uploads[]", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);

        GetMethod getFinalURLMethod = new GetMethod(uploadDataPostMethod.getResponseHeader("Location").getValue());
        getFinalURLMethod.setRequestHeader("Host", "hotfile.com");
        getFinalURLMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        getFinalURLMethod.setRequestHeader("Referer", "http://hotfile.com/");
        httpClient.executeMethod(getFinalURLMethod);

        String uploadResponse = getFinalURLMethod.getResponseBodyAsString();

        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
        return getFinalUploadedURL(uploadResponse);
    }

    private String getActionURL(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("action=\"http://") + "action=\"http://".length();
        return "http://" + uploadPageResponse.substring(uploadSuffixPosition, uploadPageResponse.indexOf("\"", uploadSuffixPosition));
    }

    private String getFinalUploadedURL(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("<input type=\"text\" name=\"url\" id=\"url\" class=\"textfield\" value=\"") + "<input type=\"text\" name=\"url\" id=\"url\" class=\"textfield\" value=\"".length();
        return uploadPageResponse.substring(uploadSuffixPosition, uploadPageResponse.indexOf("\"", uploadSuffixPosition));
    }
}
