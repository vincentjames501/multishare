package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

/**
 * User: VINCENT
 * Date: 1/3/12
 * Time: 10:54 AM
 */
public class ZShareUploadPlugin extends UploadHelper implements UploadPlugin {
    private String uploadBase;
    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        String userName = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient httpClient = new HttpClient();
        httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        PostMethod loginPostMethod = new PostMethod("http://zshare.net/myzshare/process.php?loc=http://zshare.net/myzshare/login.php");
        loginPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        loginPostMethod.setParameter("username", userName);
        loginPostMethod.setParameter("password", password);
        loginPostMethod.setParameter("submit", " Login to your account");
        httpClient.executeMethod(loginPostMethod);

        GetMethod getUploadPage = new GetMethod("http://zshare.net/index.php");
        httpClient.executeMethod(getUploadPage);

        String getUploadIdURL = getActionURL(getUploadPage.getResponseBodyAsString());

        GetMethod getUploadIdMethod = new GetMethod(getUploadIdURL);
        httpClient.executeMethod(getUploadIdMethod);

        String uploadURL = getUploadURL(getUploadIdMethod.getResponseBodyAsString());

        return "Successfully Uploaded To ZShare - Download Link Available At " + uploadFile(fileTransfer, uploadURL, httpClient);
    }

    private String getUploadURL(String uploadPageResponse) {
        int beginningIndex = uploadPageResponse.indexOf("startUpload(\"") + "startUpload(\"".length();
        return uploadBase + "cgi-bin/ubr_upload.pl?upload_id=" + uploadPageResponse.substring(beginningIndex,
                uploadPageResponse.indexOf("\"", beginningIndex)) + "&multiple=0&is_private=0&pass=&descr=";
    }

    private String uploadFile(FileTransfer fileTransfer, String actionURL, HttpClient httpClient) throws Exception {
        PostMethod uploadDataPostMethod = new PostMethod(actionURL);
        FilePart file = new FilePart("file", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);

        GetMethod getFinalUploadURLMethod = new GetMethod(uploadBase + uploadDataPostMethod.getResponseHeader("Location").getValue().substring(3));
        httpClient.executeMethod(getFinalUploadURLMethod);

        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
        return getFinalUploadURL(getFinalUploadURLMethod.getResponseBodyAsString());
    }

    private String getFinalUploadURL(String uploadResponse) {
        int beginningIndex = uploadResponse.indexOf("[URL=") + "[URL=".length();
        return uploadResponse.substring(beginningIndex, uploadResponse.indexOf("]", beginningIndex));
    }

    private String getActionURL(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("action=\"http://") + "action=\"http://".length();
        uploadBase = "http://" + uploadPageResponse.substring(uploadSuffixPosition,
                uploadPageResponse.indexOf("\"", uploadSuffixPosition));
        return uploadBase + "uberupload/ubr_link_upload.php?rnd_id=" + generateRandomNumberString(13);
    }
}
