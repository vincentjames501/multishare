package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 10:20 AM
 */
public class DepositFilesUploadPlugin extends UploadHelper implements UploadPlugin {
    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        String username = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient httpClient = new HttpClient();
        httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        PostMethod depositFilesLoginMethod = new PostMethod("http://depositfiles.com/login.php?return=/");
        depositFilesLoginMethod.setRequestHeader("Host", "depositfiles.com");
        depositFilesLoginMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        depositFilesLoginMethod.setRequestHeader("Referer", "http://depositfiles.com/");
        depositFilesLoginMethod.addParameter("go", "1");
        depositFilesLoginMethod.addParameter("login", username);
        depositFilesLoginMethod.addParameter("password", password);
        httpClient.executeMethod(depositFilesLoginMethod);

        GetMethod getUploadPageMethod = new GetMethod("http://depositfiles.com");
        depositFilesLoginMethod.setRequestHeader("Host", "depositfiles.com");
        depositFilesLoginMethod.setRequestHeader("User-Agent", "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        depositFilesLoginMethod.setRequestHeader("Referer", "Referer: http://depositfiles.com/login.php");
        httpClient.executeMethod(getUploadPageMethod);

        String uploadPageResponse = getUploadPageMethod.getResponseBodyAsString();
        String actionURL = getActionURL(uploadPageResponse);
        
        return "Successfully Uploaded To DepositFiles - Download Link Available At " + uploadFile(fileTransfer, actionURL, httpClient);
    }

    private String getUploadCheckURL(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("var file_upload_check_dir = '") + "var file_upload_check_dir = '".length();
        return uploadPageResponse.substring(uploadSuffixPosition, uploadPageResponse.indexOf("'", uploadSuffixPosition));
    }

    private String uploadFile(FileTransfer fileTransfer, String actionURL, HttpClient httpClient) throws Exception {
        PostMethod uploadDataPostMethod = new PostMethod(actionURL);
        uploadDataPostMethod.addRequestHeader("Connection", "keep-alive");
        uploadDataPostMethod.addRequestHeader("Cache-Control", "max-age=0");
        uploadDataPostMethod.addRequestHeader("Origin", "http://depositfiles.com");
        uploadDataPostMethod.addRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        uploadDataPostMethod.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        uploadDataPostMethod.addRequestHeader("Accept-Language", "en-US,en;q=0.8");
        uploadDataPostMethod.addRequestHeader("Accept-Charset", "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3");
        uploadDataPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        uploadDataPostMethod.setRequestHeader("Referer", "http://depositfiles.com");
        uploadDataPostMethod.addParameter("go", "1");
        uploadDataPostMethod.addParameter("UPLOAD_IDENTIFIER", getIdentifier(actionURL));
        uploadDataPostMethod.addParameter("MAX_FILE_SIZE", "2097152000");
        FilePart file = new FilePart("files", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);

        String uploadResponse = uploadDataPostMethod.getResponseBodyAsString();

        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
        return getFinalUploadURL(uploadResponse);
    }

    private String getFinalUploadURL(String uploadResponse) {
        int uploadSuffixPosition = uploadResponse.indexOf("parent.ud_download_url = '") + "parent.ud_download_url = '".length();
        return uploadResponse.substring(uploadSuffixPosition, uploadResponse.indexOf("'", uploadSuffixPosition));
    }

    private String getIdentifier(String actionURL) {
        return actionURL.substring(actionURL.lastIndexOf("=")+1);
    }

    private String getActionURL(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("action=\"http://") + "action=\"http://".length();
        return "http://" + uploadPageResponse.substring(uploadSuffixPosition, uploadPageResponse.indexOf("\"", uploadSuffixPosition));
    }
}
