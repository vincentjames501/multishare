package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.service.util.FileSizeFormatter;

/**
 * User: VINCENT
 * Date: 12/25/11
 * Time: 12:21 AM
 */
public class ProgressListener implements CountingMultipartRequestEntity.ProgressListener {
    private FileTransfer fileTransfer;
    private long previousTime = System.currentTimeMillis();
    private long previousBytes = 0;
    private long totalSize;

    public ProgressListener(FileTransfer fileTransfer) {
        this.fileTransfer = fileTransfer;
        this.totalSize = fileTransfer.getFileToTransfer().length();
    }

    @Override
    public void transferred(long num) {
        long newTime = System.currentTimeMillis();
        fileTransfer.setProgress(1.0f * num / totalSize);
        if ((newTime - previousTime) / 1000.0f > 1.0f) {
            fileTransfer.setSpeed(FileSizeFormatter.formatFileLength((long) ((num - previousBytes) / (((newTime - previousTime) / 1000.0f))), false) + "/s");
            previousBytes = num;
            previousTime = newTime;
        }
    }
}
