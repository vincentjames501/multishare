package multishare.service.plugins.upload;

import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.multipart.StringPart;

import java.util.Random;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 11:31 PM
 */
public class UploadHelper {
    protected String stripCookie(HttpMethod fileServePostMethod) {
        Header[] responseHeaders = fileServePostMethod.getResponseHeaders();
        String strippedCookie = "";
        for (Header responseHeader : responseHeaders) {
            if (responseHeader.getName().equals("Set-Cookie") && responseHeader.getValue().contains("cookie")) {
                if (responseHeader.getValue().contains(";")) {
                    strippedCookie += "; " + responseHeader.getValue().substring(0, responseHeader.getValue().indexOf(";"));
                } else {
                    strippedCookie += "; " + responseHeader.getValue();
                }
            }
        }
        return strippedCookie.replaceFirst("; ", "");
    }

    protected void checkResponse(String response, String checkFor, String errorMessage) throws Exception {
        if (response.contains(checkFor)) {
            throw new Exception(errorMessage);
        }
    }
    
    protected String formatCookiesIntoString(HttpClient httpClient) {
        String cookieString = "";
        for (Cookie cookie : httpClient.getState().getCookies()) {
            cookieString += cookie.getName() + "=" + cookie.getValue() + "; ";
        }

        return cookieString;
    }

    protected String generateRandomNumberString(int length) {
        String randomNumberString = "";
        Random randomNumberGenerator = new Random();
        for (int i = 0; i < length; i++) {
            randomNumberString += Integer.toString(Math.abs(randomNumberGenerator.nextInt()) % 10);
        }
        return randomNumberString;
    }

    protected StringPart buildStringPart(String name, String value) {
        StringPart stringPart = new StringPart(name, value);
        stringPart.setTransferEncoding(null);
        stringPart.setContentType(null);
        return stringPart;
    }
}
