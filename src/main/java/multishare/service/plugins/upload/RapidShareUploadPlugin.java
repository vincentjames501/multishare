package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 9:34 PM
 */
public class RapidShareUploadPlugin extends UploadHelper implements UploadPlugin {
    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        String username = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();
        
        HttpClient httpClient = new HttpClient();
        httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        GetMethod getCookieMethod = new GetMethod("https://api.rapidshare.com/cgi-bin/rsapi.cgi?sub=getaccountdetails&login="+username+"&password="+password+"&withcookie=1&cbid=1&cbf=rs.jsonp.callback");
        httpClient.executeMethod(getCookieMethod);

        String cookie = "enc=" + parseCookie(getCookieMethod.getResponseBodyAsString());

        GetMethod getUploadServerId = new GetMethod("http://rapidshare.com/cgi-bin/rsapi.cgi?sub=nextuploadserver");
        getUploadServerId.setRequestHeader("Cookie", cookie);
        httpClient.executeMethod(getUploadServerId);
        String uploadServerId = getUploadServerId.getResponseBodyAsString();
        String randomNumString = generateRandomNumberString(10);
        String uploadURL = "http://rs"+uploadServerId+".rapidshare.com/cgi-bin/rsapi.cgi?uploadid="+randomNumString;

        return "Successfully Uploaded To RapidShare - Download Link Available At " + uploadFile(fileTransfer, uploadURL, httpClient, cookie, username, password);
    }

    private String uploadFile(FileTransfer fileTransfer, String actionURL, HttpClient httpClient, String cookie, String username, String password) throws Exception {
        PostMethod uploadDataPostMethod = new PostMethod(actionURL);
        uploadDataPostMethod.addRequestHeader("Origin", "http://rapidshare.com");
        uploadDataPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        uploadDataPostMethod.setRequestHeader("Referer", "http://rapidshare.com");
        uploadDataPostMethod.setRequestHeader("Cookie", cookie);
        FilePart file = new FilePart("filecontent", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");
        Part userPart = buildStringPart("login", username);
        Part passwordPart = buildStringPart("password", password);
        Part subPart = buildStringPart("sub", "upload");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{userPart, passwordPart, subPart, file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);

        String[] response = uploadDataPostMethod.getResponseBodyAsString().split("\n")[1].split(",");

        return "http://rapidshare.com/files/"+response[0]+"/" + response[1];
    }

    private String parseCookie(String getCookieResponse) throws Exception {
        String[] values = getCookieResponse.split("\n");
        for (String value : values) {
            if(value.contains("cookie")) {
                return value.substring("cookie=".length());
            }
        }
        throw new Exception("No cookie found");
    }
}
