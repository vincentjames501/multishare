package multishare.service.plugins.upload;

import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import multishare.service.plugins.upload.api.FSApi;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 10:07 AM
 */
public class WUploadUploadPlugin extends UploadHelper implements UploadPlugin {
    private FSApi fsApi = new FSApi();

    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        return fsApi.start(fileTransfer, "api.wupload.com", "WUpload");
    }
}
