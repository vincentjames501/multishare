package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPlugin;
import org.apache.commons.httpclient.Cookie;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.cookie.CookiePolicy;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 2:03 PM
 */
public class MegaUploadUploadPlugin extends UploadHelper implements UploadPlugin {
    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        String username = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        HttpClient httpClient = new HttpClient();
        httpClient.getParams().setParameter("http.protocol.single-cookie-header", true);
        httpClient.getParams().setCookiePolicy(CookiePolicy.BROWSER_COMPATIBILITY);
        PostMethod megaUploadLoginMethod = new PostMethod("http://megaupload.com/?c=login");
        megaUploadLoginMethod.setRequestHeader("Host", "megaupload.com");
        megaUploadLoginMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        megaUploadLoginMethod.setRequestHeader("Referer", "http://megaupload.com/?c=login");
        megaUploadLoginMethod.addParameter("login", "1");
        megaUploadLoginMethod.addParameter("redir", "1");
        megaUploadLoginMethod.addParameter("username", username);
        megaUploadLoginMethod.addParameter("password", password);
        httpClient.executeMethod(megaUploadLoginMethod);


        GetMethod getUploadURLMethod = new GetMethod("http://www.megaupload.com/?login=1");
        getUploadURLMethod.setRequestHeader("Host", "megaupload.com");
        getUploadURLMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        getUploadURLMethod.setRequestHeader("Referer", "http://megaupload.com/?c=login");
        httpClient.executeMethod(getUploadURLMethod);

        String uploadURLResponse = getUploadURLMethod.getResponseBodyAsString();
        String uploadServerURL = getUploadServerURL(uploadURLResponse) + "?UPLOAD_IDENTIFIER=" + generateRandomNumberString(32) + "&user=" + getUserCookie(httpClient) + "&s=" + fileTransfer.getFileToTransfer().length();

        return uploadFile(fileTransfer, uploadServerURL, httpClient);
    }

    private String uploadFile(FileTransfer fileTransfer, String actionURL, HttpClient httpClient) throws Exception {
        PostMethod uploadDataPostMethod = new PostMethod(actionURL);
        uploadDataPostMethod.addRequestHeader("Connection", "keep-alive");
        uploadDataPostMethod.addRequestHeader("Cache-Control", "no-cache");
        uploadDataPostMethod.setRequestHeader("User-Agent", "Shockwave Flash");
        uploadDataPostMethod.addRequestHeader("Accpet", "text/*");


        FilePart file = new FilePart("Filedata", fileTransfer.getFileToTransfer());
        file.setTransferEncoding(null);
        file.setCharSet(null);
        Part filenamePart = buildStringPart("Filename", fileTransfer.getFileToTransfer().getName());
        Part userPart = buildStringPart("user", getUserCookie(httpClient));
        Part hotlinkPart = buildStringPart("hotlink", "0");
        Part uploadPart = buildStringPart("upload", "Submit Query");


        MultipartRequestEntity entity = new MultipartRequestEntity(new Part[]{filenamePart, userPart, hotlinkPart, file, uploadPart}, uploadDataPostMethod.getParams());
        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(entity, new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);

        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);

        String uploadResponse = uploadDataPostMethod.getResponseBodyAsString();

        return "Successfully Uploaded To MegaUpload - Download Link Available At " + getFinalUploadedURL(uploadResponse);
    }



    private String getFinalUploadedURL(String uploadResponse) {
        int beginningIndex = uploadResponse.indexOf("parent.downloadurl = '") + "parent.downloadurl = '".length();
        return uploadResponse.substring(beginningIndex, uploadResponse.indexOf("'", beginningIndex));
    }

    private String getUserCookie(HttpClient httpClient) {
        Cookie[] cookies = httpClient.getState().getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("user")) {
                return cookie.getValue();
            }
        }
        return "";
    }

    private String getUploadServerURL(String uploadURLResponse) {
        int beginningIndex = uploadURLResponse.indexOf("flashvars.servers = \"") + "flashvars.servers = \"".length();
        return uploadURLResponse.substring(beginningIndex, uploadURLResponse.indexOf(";", beginningIndex)) + "upload_done.php";
    }
}
