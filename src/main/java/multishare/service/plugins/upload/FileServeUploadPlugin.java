package multishare.service.plugins.upload;

import multishare.common.UploadFileTransfer;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import multishare.common.FileTransfer;
import multishare.common.UploadPlugin;

import java.io.IOException;
import java.util.Random;

public class FileServeUploadPlugin extends UploadHelper implements UploadPlugin {
    private String uploadURL;
    private String getSessionUploadURL;

    @Override
    public String start(UploadFileTransfer fileTransfer) throws Exception {
        HttpClient httpClient = new HttpClient();

        String cookie = performPostLogin(fileTransfer, httpClient);

        cookie = loadDashBoard(httpClient, cookie);

        getURLForSessionId(httpClient, cookie);

        String sessionId = getFinalUploadURL(httpClient, cookie);

        uploadFile(fileTransfer, httpClient, cookie);

        String fileServeDownloadURL = verifyFileWasUploaded(httpClient, cookie, sessionId, fileTransfer.getFileToTransfer().getName());

        return "Successfully Uploaded To FileServe - Download Link Available At " + fileServeDownloadURL;
    }



    private String verifyFileWasUploaded(HttpClient httpClient, String cookie, String sessionId, String name) throws IOException {
        String finalCheckURL = getSessionUploadURL + "&sessionId=" + sessionId;
        GetMethod getUploadSessionMethod = new GetMethod(finalCheckURL);
        getUploadSessionMethod.addRequestHeader("Cookie", cookie);
        getUploadSessionMethod.addRequestHeader("Referer", "http://fileserve.com/upload-file.php");
        httpClient.executeMethod(getUploadSessionMethod);

        return "http://www.fileserve.com/file/" + parseShortenCode(getUploadSessionMethod.getResponseBodyAsString()) + "/" + name;
    }

    private String parseShortenCode(String jsonResponse) {
        int shortenCodeBegin = jsonResponse.indexOf("shortenCode") + "shortenCode".length()+3;
        return jsonResponse.substring(shortenCodeBegin, jsonResponse.indexOf("\"", shortenCodeBegin));
    }

    private void uploadFile(FileTransfer fileTransfer, HttpClient httpClient, String cookie) throws IOException {
        PostMethod uploadDataPostMethod = new PostMethod(uploadURL);
        uploadDataPostMethod.addRequestHeader("Connection", "keep-alive");
        uploadDataPostMethod.addRequestHeader("Cache-Control", "max-age=0");
        uploadDataPostMethod.addRequestHeader("Origin", "http://fileserve.com");
        uploadDataPostMethod.addRequestHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        uploadDataPostMethod.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        uploadDataPostMethod.addRequestHeader("Accept-Language", "en-US,en;q=0.8");
        uploadDataPostMethod.addRequestHeader("Accept-Charset", "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\\r\\n");
        uploadDataPostMethod.setRequestHeader("Cookie", cookie);
        uploadDataPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        uploadDataPostMethod.setRequestHeader("Referer", "http://fileserve.com/upload-file.php");
        FilePart file = new FilePart("file", fileTransfer.getFileToTransfer());
        file.setTransferEncoding("");

        uploadDataPostMethod.setRequestEntity(new CountingMultipartRequestEntity(new MultipartRequestEntity(new Part[]{file}, uploadDataPostMethod.getParams()), new ProgressListener(fileTransfer)));
        httpClient.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
        httpClient.executeMethod(uploadDataPostMethod);
        fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
    }

    private String getFinalUploadURL(HttpClient httpClient, String cookie) throws IOException {
        GetMethod getUploadSessionMethod = new GetMethod(getSessionUploadURL);
        getUploadSessionMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        getUploadSessionMethod.addRequestHeader("Cookie", cookie);
        getUploadSessionMethod.addRequestHeader("Referer", "http://fileserve.com/upload-file.php");
        getUploadSessionMethod.addRequestHeader("Connection", "keep-alive");
        getUploadSessionMethod.addRequestHeader("Accept", "*/*");
        getUploadSessionMethod.addRequestHeader("Accept-Encoding", "gzip,deflate,sdch");
        getUploadSessionMethod.addRequestHeader("Accept-Language", "en-US,en;q=0.8");
        getUploadSessionMethod.addRequestHeader("Accept-Charset", "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.3\\r\\n");
        getUploadSessionMethod.setFollowRedirects(false);
        httpClient.executeMethod(getUploadSessionMethod);

        String uploadPageResponse = getUploadSessionMethod.getResponseBodyAsString();
        String sessionId = getSessionId(uploadPageResponse);
        uploadURL = uploadURL + sessionId + "/";
        return sessionId;
    }

    private void getURLForSessionId(HttpClient httpClient, String cookie) throws IOException {
        GetMethod getUploadFileForm = new GetMethod("http://fileserve.com/upload-file.php");
        getUploadFileForm.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        getUploadFileForm.addRequestHeader("Cookie", cookie);
        getUploadFileForm.addRequestHeader("Referer", "http://fileserve.com/dashboard.php");
        httpClient.executeMethod(getUploadFileForm);

        String uploadPageResponse = getUploadFileForm.getResponseBodyAsString();

        uploadURL = getUploadURLFromUploadPageResponse(uploadPageResponse);
        long firstRandomNumber = Math.abs(new Random().nextLong() % 1999999999999L) + 1000000000000L;
        long secondRandomNumber = Math.abs(new Random().nextLong() % 1999999999999L) + 1000000000000L;
        getSessionUploadURL = uploadURL + "?callback=jsonp" + firstRandomNumber + "&_=" + secondRandomNumber;
    }

    private String loadDashBoard(HttpClient httpClient, String cookie) throws IOException {
        GetMethod getDashboardMethod = new GetMethod("http://fileserve.com/dashboard.php");
        getDashboardMethod.setRequestHeader("Cookie", cookie);
        getDashboardMethod.setRequestHeader("Referer", "http://fileserve.com/login.php");
        getDashboardMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        httpClient.executeMethod(getDashboardMethod);

        cookie += "; " + stripCookie(getDashboardMethod);
        return cookie;
    }

    private String performPostLogin(FileTransfer fileTransfer, HttpClient httpClient) throws Exception {
        String userName = fileTransfer.getUserHost().getUsername();
        String password = fileTransfer.getUserHost().getPassword();

        PostMethod fileServeLoginPostMethod = new PostMethod("http://fileserve.com/login.php");
        fileServeLoginPostMethod.setRequestHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0) Gecko/20100101 Firefox/8.0");
        fileServeLoginPostMethod.setParameter("loginUserName", userName);
        fileServeLoginPostMethod.setParameter("loginUserPassword", password);
        fileServeLoginPostMethod.setParameter("autoLogin", "on");
        fileServeLoginPostMethod.setParameter("loginFormSubmit", "Login");
        httpClient.executeMethod(fileServeLoginPostMethod);

        String loginResponse = fileServeLoginPostMethod.getResponseBodyAsString();
        checkResponse(loginResponse, "Username doesn't exist", "Error - Username doesn't exist!");
        checkResponse(loginResponse, "Wrong password", "Error - Wrong password!");

        return stripCookie(fileServeLoginPostMethod);
    }


    private String getSessionId(String uploadPageResponse) {
        int beginSessionIndex = uploadPageResponse.indexOf("sessionId:'") + "sessionId:'".length();
        return uploadPageResponse.substring(beginSessionIndex, uploadPageResponse.indexOf("'", beginSessionIndex));
    }

    private String getUploadURLFromUploadPageResponse(String uploadPageResponse) {
        int uploadSuffixPosition = uploadPageResponse.indexOf("action=\"http://upload.fileserve.com/upload/") + "action=\"http://upload.fileserve.com/upload/".length();
        return "http://upload.fileserve.com/upload/" + uploadPageResponse.substring(uploadSuffixPosition,
                uploadPageResponse.indexOf("\"", uploadSuffixPosition));
    }


}