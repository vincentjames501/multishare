package multishare.service;

import multishare.common.Host;
import multishare.entities.EntityUploadedFile;
import multishare.entities.EntityUploadedFileHistory;
import multishare.service.adapter.UploadedFileServiceAdapter;

import javax.annotation.Resource;
import java.io.File;
import java.util.Collection;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 9:11 PM
 */
public class Notifier {
    @Resource
    private UploadedFileServiceAdapter uploadedFileServiceAdapter;

    public Notifier() {

    }

    public void updateStatus(File fileToTransfer, EntityUploadedFile uploadedFile, Host userHost, String message) {
        Collection<EntityUploadedFileHistory> uploadedFileHistoriesByUploadedFileId = uploadedFile.getUploadedFileHistoriesByUploadedFileId();
        for (EntityUploadedFileHistory entityUploadedFileHistory : uploadedFileHistoriesByUploadedFileId) {
            if(entityUploadedFileHistory.getFileHostByFileHostId().getName().equalsIgnoreCase(userHost.getFileHostByFileHostId().getName())) {
                uploadedFileServiceAdapter.updateStatus(entityUploadedFileHistory, message);
            }
        }
        if(allStatusesExist(uploadedFileHistoriesByUploadedFileId)) {
            deleteFileAndDirectory(fileToTransfer);
            //e-mail if necessary
        }
    }

    private void deleteFileAndDirectory(File fileToTransfer) {
        fileToTransfer.delete();
        fileToTransfer.getParentFile().delete();
    }

    private boolean allStatusesExist(Collection<EntityUploadedFileHistory> uploadedFileHistoriesByUploadedFileId) {
        for (EntityUploadedFileHistory entityUploadedFileHistory : uploadedFileHistoriesByUploadedFileId) {
            if(entityUploadedFileHistory.getStatus()==null) {
                return false;
            }
        }
        return true;
    }
}
