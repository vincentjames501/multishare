package multishare.service;

import multishare.common.DownloadPluginHelper;
import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.common.UploadPluginHelper;
import multishare.entities.EntityUploadedFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 8:32 PM
 */
public class FileTransferService implements Runnable {
    @Resource
    private UploadPluginHelper uploadPluginHelper;

    @Resource
    private DownloadPluginHelper downloadPluginHelper;

    private List<FileTransfer> fileTransferQueue = new ArrayList<FileTransfer>();
    private List<FileTransfer> activeTransfers = new ArrayList<FileTransfer>();

    private static final int MAX_UPLOADS = 4;
    private static final int MAX_DOWNLOADS = 4;

    public FileTransferService() {
        Executor executor = Executors.newCachedThreadPool();
        executor.execute(this);
    }

    @Override
    public void run() {
        while (true) {
            purgeCompletedTransfers();
            int currentUploads = getActiveCountByStatus(FileTransfer.Type.UPLOAD);
            int currentDownloads = getActiveCountByStatus(FileTransfer.Type.DOWNLOAD);
            while (currentUploads < MAX_UPLOADS) {
                FileTransfer nextUpload;
                if ((nextUpload = getNextTransferByType(FileTransfer.Type.UPLOAD)) != null) {
                    startNextTransfer(nextUpload);
                } else {
                    break;
                }
            }
            while (currentDownloads < MAX_DOWNLOADS) {
                FileTransfer nextDownload;
                if ((nextDownload = getNextTransferByType(FileTransfer.Type.DOWNLOAD)) != null) {
                    startNextTransfer(nextDownload);
                } else {
                    break;
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
        }
    }

    private void startNextTransfer(FileTransfer fileTransfer) {
        fileTransferQueue.remove(fileTransfer);
        activeTransfers.add(fileTransfer);
        if (fileTransfer.getTransferType() == FileTransfer.Type.UPLOAD) {
            uploadPluginHelper.startPluginForTransfer((UploadFileTransfer) fileTransfer);
        }
    }

    private FileTransfer getNextTransferByType(FileTransfer.Type type) {
        for (FileTransfer queuedTransfer : fileTransferQueue) {
            if (queuedTransfer.getTransferType() == type) {
                return queuedTransfer;
            }
        }
        return null;
    }

    private int getActiveCountByStatus(FileTransfer.Type type) {
        int count = 0;
        for (FileTransfer activeTransfer : activeTransfers) {
            if (activeTransfer.getTransferType() == type) {
                count++;
            }
        }
        return count;
    }

    private void purgeCompletedTransfers() {
        List<FileTransfer> completedTransfers = new ArrayList<FileTransfer>();
        for (FileTransfer activeTransfer : activeTransfers) {
            if (activeTransfer.getStatus() == FileTransfer.Status.COMPLETE) {
                completedTransfers.add(activeTransfer);
            }
        }
        activeTransfers.removeAll(completedTransfers);
    }

    public void queueFileTransfer(FileTransfer fileTransfer) {
        fileTransferQueue.add(fileTransfer);
    }

    public void queueFileTransfer(UploadFileTransfer fileTransfer, EntityUploadedFile uploadedFile) {
        fileTransfer.addUploadedFile(uploadedFile);
        queueFileTransfer(fileTransfer);
    }
}
