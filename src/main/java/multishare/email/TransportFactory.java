package multishare.email;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;

/**
 * User: VINCENT
 * Date: 1/4/12
 * Time: 10:32 AM
 */
public class TransportFactory {
    public Transport buildDefaultTransportObject(Session mailSession) throws NoSuchProviderException {
        return mailSession.getTransport("smtps");
    }
}
