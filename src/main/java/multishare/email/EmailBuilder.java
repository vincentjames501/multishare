package multishare.email;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.List;
import java.util.Properties;

/**
 * User: VINCENT
 * Date: 1/4/12
 * Time: 10:30 AM
 */
public class EmailBuilder {
    public static Session buildMailSession() {
        Properties props = new Properties();
        props.put("mail.smtps.auth", "true");
        return Session.getDefaultInstance(props, null);
    }

    public static MimeMessage buildEmailMessage(String subject, String content, String fromAddress, Session mailSession, List<String> adminsEmails) throws Exception {
        MimeMessage emailMessage = new MimeMessage(mailSession);
        emailMessage.setSubject("From: " + fromAddress + " - " + subject);
        emailMessage.setContent(content, "text/plain");
        for (String adminEmail : adminsEmails) {
            emailMessage.addRecipient(Message.RecipientType.TO,
                    new InternetAddress(adminEmail));
        }
        return emailMessage;
    }

    public static MimeMessage buildCustomEmailMessage(String subject, String content, String toAddress, Session mailSession) throws Exception {
        MimeMessage emailMessage = new MimeMessage(mailSession);
        emailMessage.setSubject(subject);
        emailMessage.setContent(content, "text/plain");
        if (System.getProperty("local") == null) {
            emailMessage.setFrom(new InternetAddress("multisharemultishare@gmail.com"));
        }
        emailMessage.addRecipient(Message.RecipientType.TO,
                new InternetAddress(toAddress));
        return emailMessage;
    }
}