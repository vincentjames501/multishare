package multishare.email;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

/**
 * User: VINCENT
 * Date: 1/4/12
 * Time: 10:31 AM
 */

public class EmailClient {
    private TransportFactory transportFactory = new TransportFactory();

    public void sendEmail(MimeMessage emailMessage, Session mailSession) throws Exception {
        Transport transport = transportFactory.buildDefaultTransportObject(mailSession);
        transport.connect("smtp.gmail.com", "multisharemultishare", "firestone");
        transport.sendMessage(emailMessage,
                emailMessage.getRecipients(Message.RecipientType.TO));
        transport.close();
    }
    
    public void sendEmail(String subject, String content, String toAddress) throws Exception {
        Session mailSession = EmailBuilder.buildMailSession();
        MimeMessage emailMessage = EmailBuilder.buildCustomEmailMessage(subject, content, toAddress, mailSession);

        sendEmail(emailMessage, mailSession);
    }
}
