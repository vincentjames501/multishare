package multishare.common;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:21 PM
 */
public interface UploadPlugin {
    public String start(UploadFileTransfer fileTransfer) throws Exception;
}
