package multishare.common;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:22 PM
 */
public interface DownloadPlugin {
    void start(FileTransfer fileTransfer) throws Exception;
}
