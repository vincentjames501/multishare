package multishare.common;

import multishare.entities.EntityFileHost;
import multishare.entities.EntityUserHost;

import java.io.File;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 12:16 PM
 */
public class FileTransfer {
    protected Type transferType;
    protected File fileToTransfer;
    protected Status status;
    protected Host userHost;
    protected float progress;
    protected String speed;
    protected String URL;
    protected String userName;

    public enum Status {ACTIVE, COMPLETE, QUEUED}
    public enum Type {UPLOAD, DOWNLOAD}

    public FileTransfer() {

    }

    public Type getTransferType() {
        return transferType;
    }

    public void setTransferType(Type transferType) {
        this.transferType = transferType;
    }

    public File getFileToTransfer() {
        return fileToTransfer;
    }

    public void setFileToTransfer(File fileToTransfer) {
        this.fileToTransfer = fileToTransfer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Host getUserHost() {
        return userHost;
    }

    public void setUserHost(EntityUserHost userHost) {
        this.userHost = userHost;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getSpeed() {
        return speed;
    }

    public float getProgress() {
        return progress;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
