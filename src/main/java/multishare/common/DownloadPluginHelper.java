package multishare.common;

import java.util.HashMap;
import java.util.Map;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:22 PM
 */
public class DownloadPluginHelper {
    private Map<String, Class> downloadPlugins;
    private Map<String, String> downloadPluginClasses;

    private Map<String, Class> buildPluginsMap() {
        Map<String, Class> downloadPlugins = new HashMap<String, Class>();

        for (String uploadPluginName : downloadPluginClasses.keySet()) {
            try {
                Class uploadPluginClass = Class.forName(downloadPluginClasses.get(uploadPluginName));
                downloadPlugins.put(uploadPluginName, uploadPluginClass);
            } catch (ClassNotFoundException e) {

            }
        }

        return downloadPlugins;
    }

    public void startPluginForTransfer(FileTransfer fileTransfer) {
        try {
            DownloadPlugin downloadPlugin = (DownloadPlugin) downloadPlugins.get(fileTransfer.getUserHost().getFileHostByFileHostId().getName()).newInstance();
            downloadPlugin.start(fileTransfer);
        } catch (Exception e) {
        }
    }

    public void setClasses(Map<String, String> classes) {
        this.downloadPluginClasses = classes;
        downloadPlugins = buildPluginsMap();
    }
}
