package multishare.common;

import multishare.entities.EntityFileHost;

/**
 * User: VINCENT
 * Date: 12/26/11
 * Time: 9:17 PM
 */
public interface Host {
    public String getUsername();
    public String getPassword();
    public EntityFileHost getFileHostByFileHostId();
}
