package multishare.common;

import multishare.service.Notifier;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 9:14 PM
 */
public class UploadPluginThread implements Runnable {
    private UploadFileTransfer fileTransfer;
    private UploadPlugin uploadPlugin;
    private Notifier notifier;

    public UploadPluginThread(UploadPlugin uploadPlugin, UploadFileTransfer fileTransfer, Notifier notifier) {
        this.fileTransfer = fileTransfer;
        this.uploadPlugin = uploadPlugin;
        this.notifier = notifier;
    }

    @Override
    public void run() {
        try {
            updateStatus(fileTransfer, uploadPlugin.start(fileTransfer));
        } catch (Exception e) {
            if (fileTransfer.getUploadedFile() != null) {
                fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
                notifier.updateStatus(fileTransfer.getFileToTransfer(), fileTransfer.getUploadedFile(), fileTransfer.getUserHost(), "Error: File Was Not Uploaded Successfully");
            }
        }
    }

    private void updateStatus(UploadFileTransfer fileTransfer, String message) {
        if(fileTransfer.getUploadedFile() != null) {
            fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
            notifier.updateStatus(fileTransfer.getFileToTransfer(), fileTransfer.getUploadedFile(), fileTransfer.getUserHost(), message);
        }
    }
}
