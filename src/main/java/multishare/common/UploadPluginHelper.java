package multishare.common;

import multishare.service.Notifier;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:21 PM
 */
public class UploadPluginHelper {
    private Map<String, Class> uploadPlugins;
    private Map<String, String> uploadPluginClasses;

    @Resource
    private Notifier notifier;

    private Map<String, Class> buildPluginsMap() {
        Map<String, Class> uploadPlugins = new HashMap<String, Class>();

        for (String uploadPluginName : uploadPluginClasses.keySet()) {
            try {
                Class uploadPluginClass = Class.forName(uploadPluginClasses.get(uploadPluginName));
                uploadPlugins.put(uploadPluginName, uploadPluginClass);
            } catch (ClassNotFoundException e) {

            }
        }

        return uploadPlugins;
    }

    public void startPluginForTransfer(UploadFileTransfer fileTransfer) {
        UploadPlugin uploadPlugin = null;
        try {
            uploadPlugin = (UploadPlugin) uploadPlugins.get(fileTransfer.getUserHost().getFileHostByFileHostId().getName()).newInstance();
            Executor executor = Executors.newCachedThreadPool();
            executor.execute(new UploadPluginThread(uploadPlugin, fileTransfer, notifier));
        } catch (Exception e) {
            if (fileTransfer.getUploadedFile() != null) {
                fileTransfer.setStatus(FileTransfer.Status.COMPLETE);
                notifier.updateStatus(fileTransfer.getFileToTransfer(), fileTransfer.getUploadedFile(), fileTransfer.getUserHost(), "Error: No Upload Plugin Found");
            }
        }
    }

    public void setClasses(Map<String, String> classes) {
        this.uploadPluginClasses = classes;
        uploadPlugins = buildPluginsMap();
    }
}
