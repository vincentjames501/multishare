package multishare.common;

import multishare.entities.EntityUploadedFile;

import java.io.File;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 8:19 PM
 */
public class UploadFileTransfer extends FileTransfer {
    private EntityUploadedFile uploadedFile;

    public UploadFileTransfer(File uploadFile, Host host) {
        this.transferType = Type.UPLOAD;
        this.fileToTransfer = uploadFile;
        this.userHost = host;
        this.status = Status.QUEUED;
    }

    public void addUploadedFile(EntityUploadedFile uploadedFile) {
        this.uploadedFile = uploadedFile;
    }

    public EntityUploadedFile getUploadedFile() {
        return uploadedFile;
    }
}
