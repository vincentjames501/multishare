package multishare.common;

import multishare.entities.EntityUserHost;

import java.io.File;

/**
 * User: VINCENT
 * Date: 12/27/11
 * Time: 8:24 PM
 */
public class DownloadFileTransfer extends FileTransfer {
    public DownloadFileTransfer(File file, EntityUserHost userHost, String URL) {
        this.transferType = Type.DOWNLOAD;
        this.fileToTransfer = file;
        this.userHost = userHost;
        this.URL = URL;
        this.status = Status.QUEUED;
    }
}
