package multishare.service.dao;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import multishare.entities.EntityUsers;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by IntelliJ IDEA.
 * User: vincent
 * Date: 14-Nov-2010
 * Time: 11:50:34 AM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:hibernate.cfg.xml",
        "classpath:service-context.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
@Transactional
public class DAOTestHelper {
    @Autowired
    SessionFactory sessionFactory;

    public static final String TEST_USERNAME = "thisisatestuser";

    public EntityUsers insertTestUser() {
        EntityUsers testUser = new EntityUsers();
        testUser.setEnabled(true);
        testUser.setPassword("password");
        testUser.setUsername(TEST_USERNAME);

        sessionFactory.getCurrentSession().save(testUser);
        return testUser;
    }

    @Test
    public void testSessionFactoryIsAutowired() {
        assertNotNull(sessionFactory);
    }
}
