package multishare.service.dao;

import org.junit.Before;
import org.junit.Test;
import multishare.entities.EntityUsers;

import javax.annotation.Resource;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 4:15 PM
 */
public class UserDAO_UT extends DAOTestHelper {
    @Resource
    private UserDAO userDAO;
    private EntityUsers testUser;

    @Before
    public void setUp() {
        testUser = insertTestUser();
    }

    @Test
    public void testGetAllUsers() {
        List<EntityUsers> users = userDAO.getAllUsers();
        assertTrue(users.contains(testUser));
    }

    @Test
    public void testGetUser() {
        EntityUsers user = userDAO.getUserByName("thisisatestuser");
        
        assertNotNull(user);
        assertEquals("thisisatestuser", user.getUsername());        
    }
}
