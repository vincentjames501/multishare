package multishare.service.dao;

import org.junit.Before;
import org.junit.Test;
import multishare.entities.EntityFileHost;
import multishare.entities.EntityUserHost;
import multishare.entities.EntityUsers;

import javax.annotation.Resource;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:22 AM
 */
public class FileHostDAO_UT extends DAOTestHelper{
    @Resource
    private FileHostDAO fileHostDAO;

    private EntityFileHost fileHost;
    private EntityUserHost userHost;

    @Before
    public void setUp() {
        fileHost = new EntityFileHost();
        fileHost.setName("testFileHost");

        sessionFactory.getCurrentSession().save(fileHost);

        EntityUsers testUser = insertTestUser();

        userHost = new EntityUserHost();
        userHost.setUsername("testUserName");
        userHost.setPassword("testUserName");
        userHost.setUsersByUserId(testUser);
        userHost.setFileHostByFileHostId(fileHost);

        sessionFactory.getCurrentSession().save(userHost);
    }

    @Test
    public void testGetFileHosts() throws Exception {
        List<EntityFileHost> fileHosts = fileHostDAO.getAllFileHosts();

        assertNotNull(fileHosts);
        assertTrue(fileHosts.contains(fileHost));
    }
    
    @Test
    public void testGetFileHostsByUser() throws Exception {
        List<EntityUserHost> fileHosts = fileHostDAO.getFileHostsByUser(TEST_USERNAME);

        assertNotNull(fileHosts);
        assertTrue(fileHosts.contains(userHost));
    }

    @Test
    public void testGetUserHostByName() throws Exception {
        EntityUserHost fileHosts = fileHostDAO.getUserFileHostDetailsByName(TEST_USERNAME, "testFileHost");

        assertNotNull(fileHosts);
        assertEquals(fileHosts, userHost);
    }
}
