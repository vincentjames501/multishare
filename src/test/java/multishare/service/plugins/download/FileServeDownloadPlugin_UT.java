package multishare.service.plugins.download;

import multishare.common.DownloadFileTransfer;
import org.junit.Test;
import multishare.common.FileTransfer;
import multishare.entities.EntityFileHost;
import multishare.entities.EntityUserHost;

import java.io.File;
import java.util.UUID;

/**
 * User: VINCENT
 * Date: 12/25/11
 * Time: 1:53 PM
 */
public class FileServeDownloadPlugin_UT {
    @Test
    public void testStart() throws Exception {
        FileServeDownloadPlugin downloadPlugin = new FileServeDownloadPlugin();
        EntityUserHost userHost = new EntityUserHost();
        userHost.setUsername("vincentjames501");
        userHost.setPassword("firestone");
        downloadPlugin.start(new DownloadFileTransfer(new File(UUID.randomUUID().toString()), userHost, "http://www.fileserve.com/file/symuvux"));
    }
}
