package multishare.service.plugins.download;

import multishare.common.FileTransfer;
import org.junit.Ignore;
import org.junit.Test;

/**
 * User: VINCENT
 * Date: 1/5/12
 * Time: 9:47 AM
 */
public class HTTPDownloader_UT {
    @Test
    @Ignore
    public void testDownloadForAuthenticatedHTTP_WithFileName() throws Exception {
        HTTPDownloader downloader = new HTTPDownloader();
        FileTransfer fileTransfer = new FileTransfer();
        fileTransfer.setURL("http://vincentjames501:firestone@fileserve.com/file/p9w74sN/[HorribleSubs]_One_Piece_-_527_[360p].mkv");
        downloader.start(fileTransfer);
    }
    @Test
    @Ignore
    public void testDownloadForAuthenticatedHTTP_WithOutFileName_WithForwardSlash() throws Exception {
        HTTPDownloader downloader = new HTTPDownloader();
        FileTransfer fileTransfer = new FileTransfer();
        fileTransfer.setURL("http://vincentjames501:firestone@fileserve.com/file/p9w74sN/");
        downloader.start(fileTransfer);
    }
    @Test
    @Ignore
    public void testDownloadForAuthenticatedHTTP_WithOutFileName_WithOutForwardSlash() throws Exception {
        HTTPDownloader downloader = new HTTPDownloader();
        FileTransfer fileTransfer = new FileTransfer();
        fileTransfer.setURL("http://vincentjames501:firestone@fileserve.com/file/p9w74sN");
        downloader.start(fileTransfer);
    }
    
}
