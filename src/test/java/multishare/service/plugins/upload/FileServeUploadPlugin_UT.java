package multishare.service.plugins.upload;

import multishare.common.UploadFileTransfer;
import org.junit.Test;
import multishare.common.FileTransfer;
import multishare.entities.EntityUserHost;

import java.io.File;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 2:10 PM
 */
public class FileServeUploadPlugin_UT {
    @Test
//    @Ignore
    public void testStart() throws Exception {
        FileServeUploadPlugin fileServeUploadPlugin = new FileServeUploadPlugin();
        EntityUserHost userHost = new EntityUserHost();
        userHost.setUsername("vincentjames501");
        userHost.setPassword("firestone");
        FileTransfer fileTransfer = new UploadFileTransfer(new File("C:\\Users\\VINCENT\\Music\\3 Doors Down\\Away From The Sun\\01 When I'm Gone.mp3"), userHost);
        fileTransfer.setStatus(FileTransfer.Status.ACTIVE);
        fileServeUploadPlugin.start((UploadFileTransfer) fileTransfer);
    }
}
