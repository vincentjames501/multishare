package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.entities.EntityUserHost;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertTrue;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 12:30 PM
 */
public class HotFileUploadPlugin_UT {
    @Test
    @Ignore
    public void testStart() throws Exception {
        HotFileUploadPlugin testUpload = new HotFileUploadPlugin();
        EntityUserHost userHost = new EntityUserHost();
        userHost.setUsername("user");
        userHost.setPassword("pass");
        FileTransfer fileTransfer = new UploadFileTransfer(new File("C:\\Users\\VINCENT\\Music\\3 Doors Down\\Away From The Sun\\01 When I'm Gone.mp3"), userHost);
        fileTransfer.setStatus(FileTransfer.Status.ACTIVE);
        String response = testUpload.start((UploadFileTransfer) fileTransfer);
        System.out.println(response);
        assertTrue(response.contains("http://"));
    }
}
