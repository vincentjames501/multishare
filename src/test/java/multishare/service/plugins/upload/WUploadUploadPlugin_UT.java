package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.entities.EntityUserHost;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertTrue;

/**
 * User: VINCENT
 * Date: 12/30/11
 * Time: 10:13 AM
 */
public class WUploadUploadPlugin_UT {
    @Test
    @Ignore
    public void testStart() throws Exception {
        WUploadUploadPlugin testUpload = new WUploadUploadPlugin();
        EntityUserHost userHost = new EntityUserHost();
        userHost.setUsername("user");
        userHost.setPassword("pass");
        FileTransfer fileTransfer = new UploadFileTransfer(new File("C:\\Users\\VINCENT\\Music\\3 Doors Down\\Away From The Sun\\01 When I'm Gone.mp3"), userHost);
        fileTransfer.setStatus(FileTransfer.Status.ACTIVE);
        assertTrue(testUpload.start((UploadFileTransfer) fileTransfer).contains("http://"));
    }
}
