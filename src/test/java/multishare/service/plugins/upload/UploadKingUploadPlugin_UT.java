package multishare.service.plugins.upload;

import multishare.common.FileTransfer;
import multishare.common.UploadFileTransfer;
import multishare.entities.EntityUserHost;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

import static junit.framework.Assert.assertTrue;

/**
 * User: VINCENT
 * Date: 1/3/12
 * Time: 10:45 AM
 */
public class UploadKingUploadPlugin_UT {
    @Test
    @Ignore
    public void testStart() throws Exception {
        UploadKingUploadPlugin testUpload = new UploadKingUploadPlugin();
        EntityUserHost userHost = new EntityUserHost();
        userHost.setUsername("user");
        userHost.setPassword("pass");
        FileTransfer fileTransfer = new UploadFileTransfer(new File("C:\\Users\\VINCENT\\Desktop\\nums.png"), userHost);
        fileTransfer.setStatus(FileTransfer.Status.ACTIVE);
        String uploadResponse = testUpload.start((UploadFileTransfer) fileTransfer);
        System.out.println(uploadResponse);
        assertTrue(uploadResponse.contains("http://"));
    }
}
