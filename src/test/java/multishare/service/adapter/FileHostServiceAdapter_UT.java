package multishare.service.adapter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import multishare.entities.EntityFileHost;
import multishare.service.dao.FileHostDAO;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * User: VINCENT
 * Date: 12/24/11
 * Time: 1:24 AM
 */
public class FileHostServiceAdapter_UT {
    @Mock
    private FileHostDAO fileHostDAO;

    FileHostServiceAdapter fileHostServiceAdapter;

    @Before
    public void setUp() {
        initMocks(this);
        fileHostServiceAdapter = new FileHostServiceAdapter();
        fileHostServiceAdapter.setFileHostDAO(fileHostDAO);

        EntityFileHost fileHost1 = new EntityFileHost();
        fileHost1.setName("test1");
        EntityFileHost fileHost2 = new EntityFileHost();
        fileHost2.setName("test2");
        when(fileHostDAO.getAllFileHosts()).thenReturn(Arrays.asList(fileHost1, fileHost2));
    }
    
    @Test
    public void testGetFileHosts() throws Exception {
        List<String> fileHosts = fileHostServiceAdapter.getAllFileHosts();

        assertNotNull(fileHosts);
        assertEquals(2, fileHosts.size());
        assertTrue(fileHosts.contains("test1"));
        assertTrue(fileHosts.contains("test2"));
    }
}
