package multishare.service.adapter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import multishare.entities.EntityUsers;
import multishare.service.dao.UserDAO;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * User: VINCENT
 * Date: 12/23/11
 * Time: 4:46 PM
 */
public class UserServiceAdapter_UT {
    @MockitoAnnotations.Mock
    private UserDAO userDAO;
    
    @Before
    public void setUp() {
        initMocks(this);
        EntityUsers user1 = new EntityUsers();
        user1.setUsername("user1");
        EntityUsers user2 = new EntityUsers();
        user2.setUsername("user2");
        when(userDAO.getAllUsers()).thenReturn(Arrays.asList(user1, user2));
        when(userDAO.getUserByName("user1")).thenReturn(user1);
    }
    
    @Test
    public void testGetAllUsers() throws Exception {
        List<EntityUsers> allUsers = userDAO.getAllUsers();
        
        assertNotNull(allUsers);
        assertEquals(2, allUsers.size());
    }

    @Test
    public void testGetUserByName() throws Exception {
        EntityUsers user = userDAO.getUserByName("user1");
        
        assertEquals("user1",user.getUsername());
    }
}
