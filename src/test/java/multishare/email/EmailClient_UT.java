package multishare.email;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * User: vincent
 * Date: 24-Nov-2010
 * Time: 8:05:39 PM
 */
public class EmailClient_UT {
    @Mock
    private TransportFactory transportFactory;

    @Mock
    private Transport transport;

    @Before
    public void setUp() throws NoSuchProviderException {
        initMocks(this);
        when(transportFactory.buildDefaultTransportObject(Mockito.<Session>any())).thenReturn(transport);
    }

    @Test
    public void testEmailBuilderBuildsSessionAndMessage() throws Exception {
        Session session = EmailBuilder.buildMailSession();
        MimeMessage message = EmailBuilder.buildEmailMessage("subject", "content", "fromAddress", session, new ArrayList<String>());

        assertNotNull(session);
        assertNotNull(message);
        assertEquals("From: fromAddress - subject", message.getSubject());
        assertEquals("content", message.getContent());
    }

    @Test
    public void testEmailClientSendsEmail() throws Exception {
        Session session = EmailBuilder.buildMailSession();
        MimeMessage message = EmailBuilder.buildEmailMessage("subject", "content", "fromAddress", session, new ArrayList<String>());

        EmailClient emailClient = new EmailClient();

        emailClient.sendEmail(message, session);
    }

    @Test
    @Ignore
    public void realTest() throws Exception {
        Session mailSession = EmailBuilder.buildMailSession();
        MimeMessage emailMessage = EmailBuilder.buildCustomEmailMessage("testSubject", "testContent", "multisharemultishare@gmail.com", mailSession);

        EmailClient emailClient = new EmailClient();
        emailClient.sendEmail(emailMessage, mailSession);
    }
    
    @Test
    public void parseLinks() throws Exception {
        HttpClient client = new HttpClient();
        GetMethod getMethod = new GetMethod("http://fluffy.is/downloader.php?id=16224");
        client.executeMethod(getMethod);
        String response = getMethod.getResponseBodyAsString();
        String[] split = response.split("<a href=\"");

        for(int i = 1; i < split.length; i++) {
            String link = split[i];
            System.out.println("http://fluffy.is/" + link.substring(0, link.indexOf("\"")) + "/" + link.substring(link.indexOf(">")+1, link.indexOf("<")).replace(" ", "_"));
        }
    }
}
